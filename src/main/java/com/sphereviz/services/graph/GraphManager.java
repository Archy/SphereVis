package com.sphereviz.services.graph;

import com.sphereviz.model.graph.Person;
import com.sphereviz.model.graph.SocialInteraction;
import com.sphereviz.model.json_model.JsonInteraction;
import com.sphereviz.model.json_model.JsonInteractionGroup;
import com.sphereviz.model.json_model.JsonPerson;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Service for visualization entities management
 */
public interface GraphManager {
    /**
     *
     * @return social interactions json model
     */
    @NotNull
    List<JsonInteractionGroup> getInteractionsGroups();

    /**
     *
     * @return list of all social interactions
     */
    @NotNull
    List<JsonInteraction> getInteractions();

    /**
     *
     * @return people json model
     */
    @NotNull
    List<JsonPerson> getPeople();

    /**
     *
     * @return adjacency matrix json model
     */
    @NotNull
    Map<String, Map<String, Boolean>> getGraph();

    /**
     * Updates relationship between {@link com.sphereviz.model.graph.Person} and {@link com.sphereviz.model.graph.SocialInteraction}
     *
     * @param groupId {@link com.sphereviz.model.graph.SocialInteractionGroup} id
     * @param interactionId {@link com.sphereviz.model.graph.SocialInteraction} id
     * @param memberId {@link com.sphereviz.model.graph.Person} id
     * @param newState relationship new status
     * @return resultative relationship status
     */
    boolean updateAdjacency(long groupId, long interactionId, long memberId, boolean newState);

    /**
     *
     * @return list of all {@link SocialInteraction}
     */
    @NotNull
    List<SocialInteraction> getAllSocialInteractions();

    /**
     *
     * @param name new person name
     * @param pictureNameProducer function producing picture name from created person id
     * @return person if creation was successful, null otherwise
     */
    @NotNull
    Optional<Person> createNewPerson(@NotNull final String name, @NotNull final Function<Long, String> pictureNameProducer);

    /**
     * Deletes person (ONLY from database)
     *
     * @param person person to delete
     */
    void deletePerson(@NotNull final Person person);

    /**
     * Apply changes to cached model
     *
     * @param person person that has been just added
     * @param initialInteractions interactions to which person is initially connected
     */
    void applyPersonCreation(@NotNull final Person person, @NotNull final List<Long> initialInteractions);

    /**
     * Updates social interactions hierarchy
     *
     * @param interactionId id of interaction which parent/group is to be updated
     * @param parentId id of new parent, if -1 interaction will be direct member of group
     * @param groupId new group id
     * @return true if update has been successful
     */
    boolean updateSocialInteractionsHierarchy(final long interactionId, final long parentId, final long groupId);

    /**
     *
     * @param groupName groups name to check
     * @return true if group already exists
     */
    boolean groupExists(@NotNull final String groupName);

    /**
     *
     * @param groupName groups name to be added
     * @return true if group has been added successfully
     */
    boolean addGroup(@NotNull final String groupName);

    /**
     * reruns SE algorithm
     */
    void recomputePositions();

    /**
     *
     * @param name name of interaction to check
     * @return true if interaction with given name exists
     */
    boolean interactionExists(@NotNull final String name);

    /**
     *
     * @param name name of interaction to add
     * @param groupId group to assing new interaction to
     * @param parentId parent of new interaction
     * @param people initially connected people
     * @return true if adding interaction has been successful
     */
    boolean addInteraction(@NotNull final String name, final long groupId,
                           final long parentId, @NotNull final List<Long> people);

    /**
     * Locking all locks
     */
    void lockAll();

    /**
     * Unlocking all locks, note that lockAll() method must me called first
     */
    void unlockAll();
}
