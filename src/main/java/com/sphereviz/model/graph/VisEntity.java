package com.sphereviz.model.graph;

import javax.vecmath.Vector3d;

/**
 * Base interface for every visualized entity
 */
public interface VisEntity {
    long getId();

    Vector3d getPosition();
    void setPosition(Vector3d position);

    String getName();
}
