package com.sphereviz.model.json_model;

import com.sphereviz.model.graph.Person;

import javax.vecmath.Vector3d;

/**
 * Json representation of {@link Person}
 */
public class JsonPerson {
    private String ID;
    private String name;
    private String picture;
    private Vector3d position;

    public JsonPerson(){
    }

    public JsonPerson(Person p){
        this.ID = Long.toString(p.getId());
        this.name = p.getName();
        this.picture = p.getPicture();
        this.position = p.getPosition();
    }

    public String getID() {
        return ID;
    }

    public void setID(long id) {
        this.ID = Long.toString(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Vector3d getPosition() {
        return position;
    }

    public void setPosition(Vector3d position) {
        this.position = position;
    }
}
