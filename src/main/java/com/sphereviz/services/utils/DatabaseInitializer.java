package com.sphereviz.services.utils;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsGroupsRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.data.relationship.RelationshipRepository;
import com.sphereviz.data.users.ConfirmationTokenRepository;
import com.sphereviz.data.users.RolesRepository;
import com.sphereviz.data.users.UserRepository;
import com.sphereviz.model.graph.PersonImpl;
import com.sphereviz.model.graph.SocialInteractionGroupImpl;
import com.sphereviz.model.graph.SocialInteractionImpl;
import com.sphereviz.model.relationship.RelationshipImpl;
import com.sphereviz.model.users.RoleEnum;
import com.sphereviz.model.users.RoleImpl;
import com.sphereviz.se_algorithm.SEAlgorithm;
import com.sphereviz.services.labes_generation.ImageGenerationException;
import com.sphereviz.services.labes_generation.TextImageGenerator;
import com.sphereviz.services.relationship.RelationshipManager;
import com.sphereviz.services.users.UserService;
import com.sphereviz.utils.StringSplitter;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lesscss.deps.org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Class used for populating database after the application starts for the first time.
 * Happens after spring context is created
 */
@Component
public class DatabaseInitializer {
    private static final Logger log = Logger.getLogger(DatabaseInitializer.class);

    private static final String JSON_FILE = "data.json";

    private static final String PEOPLE_ARRAY = "people";
    private static final String PERSON_NAME = "name";
    private static final String PERSON_PICTURE = "picture";

    private static final String SOCIAL_INTERACTIONS = "social-interactions";
    private static final String SUB_INTERACTIONS = "sub-interactions";
    private static final String GROUP_NAME = "name";
    private static final String GROUP_INTERACTIONS = "interactions";
    private static final String INTERACTION_NAME = "name";
    private static final String INTERACTION_RELATIONSHIPS = "relationships";


    private final SEAlgorithm seAlgorithm;
    private final RelationshipManager relationshipManager;

    private final UserService userService;
    private final UserRepository userRepository;
    private final RolesRepository rolesRepository;
    private final ConfirmationTokenRepository tokenRepository;

    private final NewPeopleRepository peopleRepository;
    private final SocialInteractionsGroupsRepository groupsRepository;
    private final SocialInteractionsRepository interactionsRepository;
    private final RelationshipRepository relationshipRepository;

    private final TextImageGenerator textImageGenerator;



    @Inject
    public DatabaseInitializer(@NotNull final SEAlgorithm seAlgorithm,
                               @NotNull final RelationshipManager relationshipManager,
                               @NotNull final UserService userService,
                               @NotNull final UserRepository userRepository,
                               @NotNull final RolesRepository rolesRepository,
                               @NotNull final ConfirmationTokenRepository tokenRepository,
                               @NotNull final NewPeopleRepository peopleRepository,
                               @NotNull final SocialInteractionsGroupsRepository groupsRepository,
                               @NotNull final SocialInteractionsRepository interactionsRepository,
                               @NotNull final RelationshipRepository relationshipRepository,
                               @NotNull final TextImageGenerator textImageGenerator) {
        this.seAlgorithm = seAlgorithm;
        this.relationshipManager = relationshipManager;
        this.userService = userService;
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
        this.tokenRepository = tokenRepository;
        this.peopleRepository = peopleRepository;
        this.groupsRepository = groupsRepository;
        this.interactionsRepository = interactionsRepository;
        this.relationshipRepository = relationshipRepository;
        this.textImageGenerator = textImageGenerator;
    }

    @PostConstruct
    public void initializeDatabase() {
//        TODO remove
        tokenRepository.deleteAll();
        relationshipRepository.deleteAll();
        userRepository.deleteAll();
        peopleRepository.deleteAll();
        interactionsRepository.deleteAll();
        groupsRepository.deleteAll();
        rolesRepository.deleteAll();

        if(rolesRepository.findAll().isEmpty()){
            for(RoleEnum roleEnum : RoleEnum.values()){
                rolesRepository.saveAndFlush(new RoleImpl(roleEnum.getName()));
            }
        }
        if(userRepository.findAll().isEmpty()) {
            userService.createAdmin();
        }

        //import data from json if necessary
        if(peopleRepository.findAll().isEmpty() && interactionsRepository.findAll().isEmpty() &&
                relationshipRepository.findAll().isEmpty() && groupsRepository.findAll().isEmpty()){
            log.info("Initializing visualization entities from json");

            final File JSONInitFile = new File(JSON_FILE);
            final File labelsFile = new File("photos/labels");

            try(
                final FileInputStream fisTargetFile = new FileInputStream(JSONInitFile)
            ) {
                if(!labelsFile.exists()) {
                    if(!labelsFile.mkdir()){
                        throw new RuntimeException("Couldn't create labels directory");
                    }
                }

                final JSONObject rootObject = new JSONObject( IOUtils.toString(fisTargetFile, "UTF-8") );
                log.info("Successfully loaded JSON Object from file...");

                loadPeople(rootObject.getJSONArray(PEOPLE_ARRAY));
                loadInteractions(rootObject.getJSONArray(SOCIAL_INTERACTIONS));

            } catch (FileNotFoundException e) {
                throw new RuntimeException("Couldn't find JSON file", e);
            } catch (IOException e) {
                throw new RuntimeException("Couldn't process JSON file", e);
            } catch (ImageGenerationException e) {
                throw new RuntimeException("Couldn't create labels image", e);
            }
            relationshipManager.loadData();
            seAlgorithm.computePositions();
        } else {
            relationshipManager.loadData();
        }

        log.info("Database initialized");
    }

    private void loadPeople(final JSONArray people) {
        for (int i=0; i< people.length(); ++i) {
            final JSONObject person = people.getJSONObject(i);

            final PersonImpl entity = new PersonImpl(
                   person.getString(PERSON_NAME),
                   person.has(PERSON_PICTURE) ? person.getString(PERSON_PICTURE) : null
            );
            peopleRepository.save(entity);
        }
        peopleRepository.flush();
    }

    private void loadInteractions(final JSONArray groups) throws ImageGenerationException {
        for (int i=0; i< groups.length(); ++i) {
            final JSONObject group = groups.getJSONObject(i);

            final SocialInteractionGroupImpl entity = new SocialInteractionGroupImpl(
                group.getString(GROUP_NAME)
            );
            groupsRepository.save(entity);

            loadGroup(group.getJSONArray(GROUP_INTERACTIONS), entity);
        }
        groupsRepository.flush();
        interactionsRepository.flush();
        relationshipRepository.flush();
    }

    private void loadGroup(final JSONArray interactions, final SocialInteractionGroupImpl group) throws ImageGenerationException {
        for (int i=0; i< interactions.length(); ++i) {
            final JSONObject interaction = interactions.getJSONObject(i);

            final SocialInteractionImpl entity = new SocialInteractionImpl(
                    interaction.getString(INTERACTION_NAME),
                    group
            );
            group.addSocialInteractionToGroup(entity);
            interactionsRepository.save(entity);
            textImageGenerator.generate(StringSplitter.splitString(entity.getName()), String.valueOf(entity.getId()));

            loadRelationships(interaction.getJSONArray(INTERACTION_RELATIONSHIPS), entity);
            if(interaction.has(SUB_INTERACTIONS)){
                loadSubInteractions(interaction.getJSONArray(SUB_INTERACTIONS), entity);
            }
        }
    }

    private void loadSubInteractions(final JSONArray interactions, final SocialInteractionImpl parent) throws ImageGenerationException {
        for (int i=0; i< interactions.length(); ++i) {
            final JSONObject interaction = interactions.getJSONObject(i);

            final SocialInteractionImpl entity = new SocialInteractionImpl(
                    interaction.getString(INTERACTION_NAME),
                    parent
            );
            parent.addSubInteraction(entity);
            interactionsRepository.save(entity);
            textImageGenerator.generate(StringSplitter.splitString(entity.getName()), String.valueOf(entity.getId()));

            loadRelationships(interaction.getJSONArray(INTERACTION_RELATIONSHIPS), entity);
            if(interaction.has(SUB_INTERACTIONS)){
                loadSubInteractions(interaction.getJSONArray(SUB_INTERACTIONS), entity);
            }
        }
    }

    private void loadRelationships(final JSONArray relationships, final SocialInteractionImpl interaction) {
        for(int i=0; i<relationships.length(); ++i){
            final String personName = relationships.getString(i);
            final PersonImpl person = peopleRepository.findByName(personName);

            final RelationshipImpl entity = new RelationshipImpl(
                    person, interaction
            );
            relationshipRepository.save(entity);
        }
    }
}
