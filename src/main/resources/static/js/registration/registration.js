var registrationModule = angular.module('registration', []);

registrationModule.controller('mainController', function ($scope, $http, $location) {
    $scope.notUnique = true;
    $scope.displayMessage  = false;
    $scope.checkingUniqueness = false;
    $scope.emailInput = "";
});

registrationModule.directive('ngUniqueEmail', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            notUnique: '=',
            displayMessage: '=',
            checkingUniqueness: '='
        },
        controller: ["$scope", function ($scope, $http) {
        }],
        link: function (scope, element, attrs, ngModel) {
            element.bind('blur', function (e) {
                if (!ngModel || !element.val()) return;
                var currentValue = element.val();
                scope.checkingUniqueness = true;

                $http.get('/rest/email', {
                    params: {'email' : element.val()}
                }).then(
                    function (unique) {
                        //Ensure value that being checked hasn't changed
                        if (currentValue === element.val() && unique.data === true) {
                            //ngModel.$setValidity('unique', unique);
                            scope.notUnique = false;
                            scope.displayMessage  = false;
                        } else {
                            scope.notUnique = true;
                            scope.displayMessage  = true;
                        }
                        scope.checkingUniqueness = false;
                    },
                    function (unique) {
                        scope.notUnique = true;
                        scope.displayMessage = true;
                        scope.checkingUniqueness = false;
                    }
                );
            });
        }
    }
}]);

angular.module('socialViz')
    .directive('ngUniqueElement', function ($http, $location) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                token: '@',
                notUnique: '=',
                displayMessage: '=',
                checkingUniqueness: '=',
                url: '@',
                paramName: '@'
            },
            controller: ['$scope', function ngUniquenessController($scope) {
            }],
            link: function link(scope, element, attrs, ngModel) {
                element.bind('blur', function (e) {
                    if (!ngModel || !element.val()) return;
                    var currentValue = element.val();
                    scope.checkingUniqueness = true;

                    var fd = new FormData();
                    fd.append(scope.paramName , currentValue);

                    $http({
                        method: 'POST',
                        url: $location.protocol() + '://' + location.host + scope.url,
                        data: fd,
                        headers: {
                            'X-CSRF-TOKEN': scope.token,
                            'Content-Type': undefined
                        }
                    }).then(
                        function (unique) {
                            //Ensure value that being checked hasn't changed
                            if (currentValue === element.val() && unique.data === true) {
                                scope.notUnique = false;
                                scope.displayMessage = false;
                            } else {
                                scope.notUnique = true;
                                scope.displayMessage = true;
                            }
                            scope.checkingUniqueness = false;
                        },
                        function (unique) {
                            scope.notUnique = true;
                            scope.displayMessage = true;
                            scope.checkingUniqueness = false;
                        }
                    );
                });
            }
        };
    });