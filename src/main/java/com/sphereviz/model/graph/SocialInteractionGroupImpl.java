package com.sphereviz.model.graph;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(
        name = "SOCIAL_INTERACTION_GROUP"
)
public class SocialInteractionGroupImpl implements SocialInteractionGroup {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "NAME")
    private String name;
    @OneToMany(
            targetEntity = SocialInteractionImpl.class,
            fetch = FetchType.EAGER,
            mappedBy = "group"
    )
    private Set<SocialInteraction> socialInteractions = new HashSet<>();


    public SocialInteractionGroupImpl() {
    }

    public SocialInteractionGroupImpl(String name) {
        this.name = name;
    }

    public SocialInteractionGroupImpl(String name, Set<SocialInteraction> socialInteractions) {
        this.name = name;
        this.socialInteractions = socialInteractions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SocialInteraction> getSocialInteractions() {
        return socialInteractions;
    }

    public void setSocialInteractions(Set<SocialInteraction> socialInteractions) {
        this.socialInteractions = socialInteractions;
    }

    public void addSocialInteractionToGroup(SocialInteraction socialInteraction){
        socialInteractions.add(socialInteraction);
    }
}
