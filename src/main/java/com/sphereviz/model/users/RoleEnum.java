package com.sphereviz.model.users;

/**
 * Enumeration of existing user roles
 */
public enum RoleEnum{
    ADMIN("ROLE_ADMIN"),
    SUBSCRIBER("ROLE_SUBSCRIBER"),
    UNCONFIRMED("ROLE_UNCONFIRMED"),
    UNASSIGNED("ROLE_UNASSIGNED");

    final String name;

    RoleEnum(String name){
        this.name=name;
    }

    public static RoleEnum getEnum(String value) {
        for(RoleEnum v : values())
            if(v.getName().equalsIgnoreCase(value)) return v;
        throw new IllegalArgumentException();
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}