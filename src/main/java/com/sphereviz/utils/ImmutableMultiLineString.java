package com.sphereviz.utils;

import java.util.List;

/**
 * Immutable representation of {@link MultiLineString}
 */
public interface ImmutableMultiLineString {
    /**
     *
     * @return total count of all lines
     */
    int getLinesCount();

    /**
     *
     * @return width of longest line
     */
    int getMaxLineWidth();

    /**
     *
     * @return immutable list of all lines
     */
    List<String> getLines();

    String toString();
}
