package com.sphereviz.model.helpers;

/**
 * Model used for receiving adjacency update in {@link com.sphereviz.controllers.adjacency.AdjacencyREST}
 */
public class UpdateJson {
    private long group;
    private long interaction;
    private long member;
    private boolean checked;

    public long getGroup() {
        return group;
    }

    public void setGroup(long group) {
        this.group = group;
    }

    public long getInteraction() {
        return interaction;
    }

    public void setInteraction(long interaction) {
        this.interaction = interaction;
    }

    public long getMember() {
        return member;
    }

    public void setMember(long member) {
        this.member = member;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
