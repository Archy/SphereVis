package com.sphereviz.security;


import com.sphereviz.security.filters.CSRFHeaderFilter;
import com.sphereviz.security.filters.UnconfirmedUserRedirectFilter;
import com.sphereviz.security.helpers.SpVisUserDetailsService;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

/**
 * Spring security configuration
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final Logger log = Logger.getLogger(SecurityConfig.class);

    private final SpVisUserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    private final CSRFHeaderFilter csrfHeaderFilter;
    private final UnconfirmedUserRedirectFilter unconfirmedUserRedirectFilter;


    @Inject
    public SecurityConfig(@NotNull final SpVisUserDetailsService userDetailsService,
                          @NotNull final PasswordEncoder passwordEncoder,
                          @NotNull final CSRFHeaderFilter csrfHeaderFilter,
                          @NotNull final UnconfirmedUserRedirectFilter unconfirmedUserRedirectFilter) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.csrfHeaderFilter = csrfHeaderFilter;
        this.unconfirmedUserRedirectFilter = unconfirmedUserRedirectFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //add filter so CSRF token is handled properly by Angular and Spring
        http.addFilterAfter(csrfHeaderFilter, BasicAuthenticationFilter.class);
        //redirect user with unconfirmed email
        http.addFilterAfter(unconfirmedUserRedirectFilter, UsernamePasswordAuthenticationFilter.class);

        //confiugre security
        http
            .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/home.html").permitAll()
                .antMatchers("/login/*").permitAll()
                .antMatchers("/registration").permitAll()
                .antMatchers("/registration/*").permitAll()
                .antMatchers("/rest/*").permitAll()
                .antMatchers("/photos/**").permitAll()
                .anyRequest().authenticated()
            .and()
            .formLogin()
                .loginPage("/login").permitAll()
                .failureUrl("/login/failure")
                .defaultSuccessUrl("/", true)
            .and()
            .logout()
                .logoutUrl("/logout").logoutSuccessUrl("/").permitAll();

        log.info("Spring security configured");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Bean
    DefaultAuthenticationEventPublisher defaultAuthenticationEventPublisher() {
        return new DefaultAuthenticationEventPublisher();
    }
}
