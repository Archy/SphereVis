package com.sphereviz.events.types;

import com.sphereviz.model.users.User;
import org.springframework.context.ApplicationEvent;

import javax.validation.constraints.NotNull;

/**
 * Event occuring after user have successfully registered, so confirmation email can be send
 */
public class OnRegistrationCompleteEvent extends ApplicationEvent {
    private final String url;
    private final User user;


    public OnRegistrationCompleteEvent(@NotNull final String url,
                                       @NotNull final User user) {
        super(user);
        this.url = url;
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public User getUser() {
        return user;
    }
}
