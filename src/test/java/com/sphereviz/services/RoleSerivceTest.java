package com.sphereviz.services;

import com.sphereviz.data.users.RolesRepository;
import com.sphereviz.model.users.Role;
import com.sphereviz.model.users.RoleEnum;
import com.sphereviz.services.users.RoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

/**
 * Tests for {@link com.sphereviz.services.users.RoleServiceImpl}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleSerivceTest {
    @Autowired
    private RoleService roleService;
    @Autowired
    private RolesRepository rolesRepository;

    @Test
    public void findAll() {
        List<Role> roles = roleService.findAllByName(RoleEnum.ADMIN, RoleEnum.SUBSCRIBER,
                                                        RoleEnum.UNASSIGNED, RoleEnum.UNCONFIRMED);
        assertThat(roles.size(), is(4));
        assertThat(roles, containsInAnyOrder(
                rolesRepository.findAll().toArray()
        ));
    }
}
