package com.sphereviz.model.relationship;

import com.sphereviz.model.graph.Person;
import com.sphereviz.model.graph.PersonImpl;
import com.sphereviz.model.graph.SocialInteraction;
import com.sphereviz.model.graph.SocialInteractionImpl;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(
        name = "RELATIONSHIP"
)
public class RelationshipImpl implements Relationship {
    @Id
    @GeneratedValue(strategy=IDENTITY)
    private long id;
    @ManyToOne(
            targetEntity = PersonImpl.class,
            fetch = FetchType.EAGER,
            optional = false
    )
    @JoinColumn(
            name = "PERSON_ID",
            nullable = false
    )
    private Person person;
    @ManyToOne(
            targetEntity = SocialInteractionImpl.class,
            fetch = FetchType.EAGER,
            optional = false
    )
    @JoinColumn(
            name = "SOCIAL_INTERACTION_ID",
            nullable = false
    )
    private SocialInteraction socialInteraction;


    public RelationshipImpl() {
    }

    public RelationshipImpl(Person person, SocialInteraction socialInteraction) {
        this.person = person;
        this.socialInteraction = socialInteraction;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public SocialInteraction getSocialInteraction() {
        return socialInteraction;
    }

    public void setSocialInteraction(SocialInteraction socialInteraction) {
        this.socialInteraction = socialInteraction;
    }

    @Override
    public long getInteractionId() {
        return socialInteraction.getId();
    }

    @Override
    public long getPersonId() {
        return person.getId();
    }
}
