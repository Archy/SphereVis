package com.sphereviz.se_algorithm;

import com.sphereviz.model.graph.SocialInteraction;
import com.sphereviz.model.graph.VisEntity;
import com.sphereviz.services.graph.EntityService;
import com.sphereviz.utils.ListCreator;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.NotThreadSafe;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.vecmath.Vector3d;
import java.util.List;
import java.util.Random;


@NotThreadSafe
@Component
public class SEAlgorithmImpl implements SEAlgorithm {
    private static final Logger log = Logger.getLogger(SEAlgorithmImpl.class);

    private static final int ALGORITHM_RUNS = 30;
    private static final double DEGREE = 0.01;
    private static final double RA = 18.0;
    private static final double RB = 30.0;

    private final EntityService entityService;

    private List<VisEntity> visEntities;
    private List<Vector3d> gradient;


    @Inject
    public SEAlgorithmImpl(@NotNull final EntityService entityService) {
        this.entityService = entityService;
    }

    @Override
    public void computePositions() {
        //get entities from database
        visEntities = entityService.getAllEntities();

        //set initial positions
        setInitPositions();

        for(int i=0; i<ALGORITHM_RUNS; ++i){
            log.info("SE algorithm run: '" + i + "' starts");
            //count gradient
            countGradient();
            //update positions
            int index = 0;
            for(VisEntity entity : visEntities){
                final Vector3d grad = gradient.get(index);
                final Vector3d pos = entity.getPosition();
                //project gradient onto the tangent plane
                double dotProduct = grad.dot(pos);
                final Vector3d h = new Vector3d(
                        grad.getX() - dotProduct*pos.getX(),
                        grad.getY() - dotProduct*pos.getY(),
                        grad.getZ() - dotProduct*pos.getZ()
                );
                // normalize it
                double radius = entity instanceof SocialInteraction ? RA : RB;
                h.normalize();
                h.scale(radius);
                //count new positions
                final Vector3d newPosition = new Vector3d(
                        pos.getX() * Math.cos(DEGREE) - h.getX() * Math.sin(DEGREE),
                        pos.getY() * Math.cos(DEGREE) - h.getY() * Math.sin(DEGREE),
                        pos.getZ() * Math.cos(DEGREE) - h.getZ() * Math.sin(DEGREE)
                );
                newPosition.normalize();
                newPosition.scale(radius);
                entity.setPosition(newPosition);

                ++index;
            }
            log.info("SE algorithm run: '" + i + "' finished");
        }

        entityService.safeAllEntitiesPositions();
    }

    private void setInitPositions(){
        final Random generator = new Random(System.currentTimeMillis());

        for(VisEntity entity : visEntities){

            final double theta = generator.nextDouble() * 2.0 * Math.PI;
            final double phi = generator.nextDouble() * Math.PI;

            if(entity instanceof SocialInteraction){
                entity.setPosition(new Vector3d(
                        RA * Math.sin(phi) * Math.cos(theta), RA * Math.cos(phi), RA * Math.sin(phi) * Math.sin(theta)
                ));
            } else {
                entity.setPosition(new Vector3d(
                        RB * Math.sin(phi) * Math.cos(theta), RB * Math.cos(phi), RB * Math.sin(phi) * Math.sin(theta)
                ));
            }
        }
    }

    private void countGradient(){
        gradient = ListCreator.createList(visEntities.size(), Vector3d::new);
        for(int i=0; i<visEntities.size()-1; ++i) {
            for(int j=i+1; j<visEntities.size(); ++j){
                final VisEntity e1 = visEntities.get(i);
                final VisEntity e2 = visEntities.get(j);

                final double sum = getARiRj(e1, e2) - e1.getPosition().dot(e2.getPosition());

                updateGradient(gradient.get(i), e2.getPosition(), sum);
                updateGradient(gradient.get(j), e1.getPosition(), sum);
            }
        }
        log.info("Gradient counted");
    }

    private double getARiRj(final VisEntity e1, final VisEntity e2){
        double ARiRj = 1.0;

        ARiRj *= e1 instanceof SocialInteraction ? RA : RB;
        ARiRj *= e2 instanceof SocialInteraction ? RA : RB;

        if(!entityService.areConnected(e1, e2))
            ARiRj *= -1.0;

        return ARiRj;
    }

    private void updateGradient(final Vector3d grad, final Vector3d pos, final double sum){
        grad.setX(grad.getX() - sum*pos.getX());
        grad.setY(grad.getY() - sum*pos.getY());
        grad.setZ(grad.getZ() - sum*pos.getZ());
    }
}
