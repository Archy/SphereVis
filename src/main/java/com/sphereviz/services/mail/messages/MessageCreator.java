package com.sphereviz.services.mail.messages;

/**
 * Confirmation email content creator
 */
public class MessageCreator{
    public static String createConfirmationMessage(final String url, final String email) {
        final StringBuilder msg = new StringBuilder();
        msg.append("<p>");
        msg.append("Hello ");
        msg.append(email);
        msg.append("</p>");

        msg.append("Please follow this link to confirm your email:\n");
        msg.append("<a href=\"");
        msg.append(url);
        msg.append("\">confirm</a>");

        return msg.toString();
    }
}
