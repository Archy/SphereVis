package com.sphereviz.services;

import com.sphereviz.data.relationship.RelationshipRepository;
import com.sphereviz.model.relationship.Relationship;
import com.sphereviz.services.relationship.RelationshipManager;
import com.sphereviz.services.relationship.RelationshipManagerImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Tests for {@link RelationshipManagerImpl}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RelationshipManagerTest {
    @Autowired
    private RelationshipManager relationshipManager;
    @Autowired
    private RelationshipRepository relationshipRepository;

    @Test
    public void relationshipsExists(){
        List<? extends Relationship> relationships = relationshipRepository.findAll();

        for(Relationship relationship : relationships) {
            assertThat(
                    relationshipManager.relationshipExists(relationship.getPersonId(), relationship.getInteractionId()),
                    is(true)
            );
        }
    }

    @Test
    public void updateRelationships(){
        List<? extends Relationship> relationships = relationshipRepository.findAll();

        //delete all
        for(Relationship relationship : relationships) {
            assertThat(
                    relationshipManager.relationshipExists(relationship.getPersonId(), relationship.getInteractionId()),
                    is(true)
            );
            boolean result = relationshipManager.updateRelationship(relationship.getPersonId(), relationship.getInteractionId(), false);
            assertThat(result, is(false));
        }
    }

}
