package com.sphereviz.services.relationship;

import com.sphereviz.model.graph.VisEntity;

/**
 * Service, managing relationships between {@link com.sphereviz.model.graph.Person}
 * and {@link com.sphereviz.model.graph.SocialInteraction}
 */
public interface RelationshipManager {
    /**
     * Loads all {@link com.sphereviz.model.graph.SocialInteractionImpl} to memory
     */
    void loadData();

    /**
     * Checks if relationship between 2 entities exists, entities can be of the same type
     * @param e1 entity of type {@link com.sphereviz.model.graph.SocialInteraction} or {@link com.sphereviz.model.graph.Person}
     * @param e2 entity of type {@link com.sphereviz.model.graph.SocialInteraction} or {@link com.sphereviz.model.graph.Person}
     * @return true if relationship between entities exists
     */
    boolean relationshipExists(VisEntity e1, VisEntity e2);

    /**
     * Checks if relationship between person and social interaction exists
     * @param personId person id
     * @param interactionId social interaction id
     * @return true if relationship between entities exists
     */
    boolean relationshipExists(long personId, long interactionId);

    /**
     * Change relationship status
     * @param interactionId interaction id
     * @param personId person id
     * @param newRelationshipStatus relationship new status (false->non-existent, true->existent)
     * @return resultative relationship status
     */
    boolean updateRelationship(long interactionId, long personId, boolean newRelationshipStatus);
}
