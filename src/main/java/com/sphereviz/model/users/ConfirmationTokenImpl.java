package com.sphereviz.model.users;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(
        name = "confirmation_token"
)
public class ConfirmationTokenImpl implements ConfirmationToken {
    @Id
    @GeneratedValue(strategy=IDENTITY)
    private long id;
    @Column
    private String token;
    @OneToOne(fetch = FetchType.EAGER, targetEntity = UserImpl.class)
    @JoinColumn(name = "user_id")
    private User user;


    public ConfirmationTokenImpl(){
    }

    public ConfirmationTokenImpl(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
