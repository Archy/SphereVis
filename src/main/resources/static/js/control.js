﻿var vizModule = angular.module('socialViz', ["ngRoute"]);

vizModule.service('syncSRV', function ($rootScope) {
    "use strict";

    this.sync = function (data) {
        this.syncData = data;
        $rootScope.$broadcast('updated');
    };

    this.syncScroll = function (data) {
        this.scrollTo = data;
        $rootScope.$broadcast('updatedScroll');
    };

    this.syncSocialInteractions = function (type) {
        this.interactionType = type;
        $rootScope.$broadcast('updatedSocialInteraction');
    };

    this.beginSpheresSwitch = function () {
        $rootScope.$broadcast('startSpheresSwitch');
    };

    this.adjacencyNeedsReloadSrv = function () {
        $rootScope.adjacencyNeedsReload = true;
    };
});

vizModule.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "home.html"
        })
        .when("/adjacency", {
            cache: false,
            templateUrl : "/adjacency"
        })
        .when("/interactions/add-interactions", {
            templateUrl : "/interactions/add-interactions"
        })
        .when("/interactions/add-group", {
            templateUrl : "/interactions/add-group"
        })
        .when("/interactions/hierarchy", {
            templateUrl : "/interactions/hierarchy"
        });
});

vizModule.directive("ngHeightManager", function ($route, $routeParams, $location, $rootScope, $templateCache) {
    return {
        restrict: "EA",
        controller: ["$scope", function ($scope, $rootScope) {
        }],
        link: function postLink(scope, element, attrs) {
            scope.$on("$routeChangeSuccess",
                function handleRouteChangeEvent( event, current ) {
                    console.log( "Route Change: " + current );
                    if(current.originalPath === "/adjacency" && $rootScope.adjacencyNeedsReload===true){
                        console.warn("reload");
                        var currentPageTemplate = $route.current.templateUrl;
                        $templateCache.remove(currentPageTemplate);
                        $route.reload();
                        $rootScope.adjacencyNeedsReload = false;
                    }
                }
            );
        }
    }
});
