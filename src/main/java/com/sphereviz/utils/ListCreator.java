package com.sphereviz.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Creates list of instances
 */
public class ListCreator {
    private ListCreator(){}

    public static <T> List<T> createList(final int size, Supplier<T> supplier){
        List<T> list = new ArrayList<>();

        for(int i=0; i<size; ++i){
            list.add(supplier.get());
        }

        return list;
    }

}
