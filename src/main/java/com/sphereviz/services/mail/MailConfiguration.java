package com.sphereviz.services.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;


@Configuration
@PropertySources({
        @PropertySource("classpath:mail.properties"),
        @PropertySource("classpath:password.properties")
})
public class MailConfiguration {

    @Value("${mail.host}")
    private String host;
    @Value("${mail.port}")
    private int port;
    @Value("${mail.username}")
    private String username;
    @Value("${mail.protocol}")
    private String protocol;
    @Value("${mail.smtp.starttls.enable}")
    private boolean starttls;
    @Value("${mail.default-encoding}")
    private String encoding;
    @Value("${mail.smtp.ssl.trust}")
    private String trust;

    @Value("${mail.password}")
    private String password;

    @Bean
    public JavaMailSender javaMailSender() {
        final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        final Properties mailProperties = new Properties();

        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        mailSender.setProtocol(protocol);
        mailProperties.put("mail.smtp.starttls.enable", starttls);
        mailProperties.put("mail.default-encoding", encoding);
        mailProperties.put("mail.smtp.ssl.trust", trust);

        mailSender.setJavaMailProperties(mailProperties);
        return mailSender;
    }
}