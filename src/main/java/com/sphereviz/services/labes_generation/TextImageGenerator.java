package com.sphereviz.services.labes_generation;

import com.sphereviz.utils.ImmutableMultiLineString;

import javax.validation.constraints.NotNull;

/**
 * Component that generates images
 */
public interface TextImageGenerator {
    /**
     * Generates 2 files: one with normal and one with highlighted text
     *
     * @param text text to be rendered in file
     * @param fileName file name to save normal text, highlighted text is saved to 'highlighted.filename'
     */
    void generate(@NotNull final ImmutableMultiLineString text, @NotNull final String fileName) throws ImageGenerationException;
}
