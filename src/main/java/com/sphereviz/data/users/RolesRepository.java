package com.sphereviz.data.users;

import com.sphereviz.model.users.RoleImpl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * {@link RoleImpl} repository
 */
public interface RolesRepository extends JpaRepository<RoleImpl, Long> {
    RoleImpl findByName(String name);

    List<RoleImpl> findAllByName(String name);
}
