package com.sphereviz.services.graph;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsGroupsRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.model.graph.Person;
import com.sphereviz.model.graph.PersonImpl;
import com.sphereviz.model.graph.SocialInteraction;
import com.sphereviz.model.graph.SocialInteractionGroup;
import com.sphereviz.model.graph.SocialInteractionGroupImpl;
import com.sphereviz.model.graph.SocialInteractionImpl;
import com.sphereviz.model.json_model.JsonInteraction;
import com.sphereviz.model.json_model.JsonInteractionGroup;
import com.sphereviz.model.json_model.JsonPerson;
import com.sphereviz.se_algorithm.SEAlgorithm;
import com.sphereviz.services.labes_generation.ImageGenerationException;
import com.sphereviz.services.labes_generation.TextImageGenerator;
import com.sphereviz.services.relationship.RelationshipManager;
import com.sphereviz.utils.StringSplitter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class GraphManagerImpl implements GraphManager {
    private static final Logger log = Logger.getLogger(GraphManagerImpl.class);
    private static final long NO_PARENT = -1;

    private final ReentrantLock graphLock = new ReentrantLock();
    private final ReentrantLock interactionLock = new ReentrantLock();
    private final ReentrantLock allInteractionsLock = new ReentrantLock();
    private final ReentrantLock groupsLock = new ReentrantLock();
    private final ReentrantLock peopleLock = new ReentrantLock();

    private final SEAlgorithm seAlgorithm;
    private final RelationshipManager relationshipManager;
    private final TextImageGenerator textImageGenerator;

    private final NewPeopleRepository peopleRepository;
    private final SocialInteractionsGroupsRepository groupsRepository;
    private final SocialInteractionsRepository socialInteractionsRepository;

    private List<JsonInteractionGroup> interactionsGroupsJson = null;
    private List<JsonInteraction> interactionsJson = null;
    private List<JsonPerson> peopleJson = null;
    private Map<String, Map<String, Boolean>> graphJson = null;


    @Inject
    public GraphManagerImpl(@NotNull final SEAlgorithm seAlgorithm,
                            @NotNull final  RelationshipManager relationshipManager,
                            @NotNull final NewPeopleRepository peopleRepository,
                            @NotNull final SocialInteractionsGroupsRepository groupsRepository,
                            @NotNull final SocialInteractionsRepository socialInteractionsRepository,
                            @NotNull final TextImageGenerator textImageGenerator) {
        this.seAlgorithm = seAlgorithm;
        this.relationshipManager = relationshipManager;
        this.peopleRepository = peopleRepository;
        this.groupsRepository = groupsRepository;
        this.socialInteractionsRepository = socialInteractionsRepository;
        this.textImageGenerator = textImageGenerator;
    }

    @Override
    @NotNull
    public List<JsonInteractionGroup> getInteractionsGroups() {
        try {
            groupsLock.lock();
            if(interactionsGroupsJson == null){
                interactionsGroupsJson = new ArrayList<>();

                for (SocialInteractionGroup group : groupsRepository.findAll()) {
                    final List<JsonInteraction> jsonInteractions = group.getSocialInteractions()
                            .stream()
                            .map(JsonInteraction::new)
                            .collect(Collectors.toList());
                    interactionsGroupsJson.add(new JsonInteractionGroup(String.valueOf(group.getId()), group.getName(), jsonInteractions));
                }

                log.info("Social interactions json list created");
            }
            return interactionsGroupsJson;
        } finally {
            groupsLock.unlock();
        }
    }

    @Override
    @NotNull
    public List<JsonInteraction> getInteractions() {
        try {
            interactionLock.lock();
            if (interactionsJson == null) {
                interactionsJson = socialInteractionsRepository.findAll()
                        .stream()
                        .map(JsonInteraction::new)
                        .collect(Collectors.toList());
            }
            return interactionsJson;
        } finally {
            interactionLock.unlock();
        }
    }

    @Override
    @NotNull
    public List<JsonPerson> getPeople() {
        try {
            peopleLock.lock();
            if(peopleJson == null){
                peopleJson = peopleRepository.findAll().stream()
                                                .map(JsonPerson::new)
                                                .collect(Collectors.toList());
                log.info("People json list created");
            }
            return peopleJson;
        } finally {
            peopleLock.unlock();
        }
    }

    @Override
    @NotNull
    public Map<String, Map<String, Boolean>> getGraph() {
        try {
            graphLock.lock();
            if(graphJson == null){
                graphJson = new HashMap<>();
                for(Person person : peopleRepository.findAll()){
                    final Map<String, Boolean> interactionAdjacency = new HashMap<>();
                    for(SocialInteraction interaction : socialInteractionsRepository.findAll()) {
                        interactionAdjacency.put(
                                String.valueOf(interaction.getId()),
                                relationshipManager.relationshipExists(person.getId(), interaction.getId())
                        );
                    }
                    graphJson.put(String.valueOf(person.getId()), interactionAdjacency);
                }
                log.info("Adjacency json graph created");
            }
            return graphJson;
        } finally {
            graphLock.unlock();
        }
    }

    @Override
    @NotNull
    public List<SocialInteraction> getAllSocialInteractions() {
        try{
            allInteractionsLock.lock();
            return new ArrayList<>(socialInteractionsRepository.findAll());
        } finally {
            allInteractionsLock.unlock();
        }

    }

    @Override
    public boolean updateAdjacency(final long groupId, final long interactionId,
                                   final long memberId, final boolean newState) {
        try {
            graphLock.lock();
            boolean outcome = relationshipManager.updateRelationship(interactionId, memberId, newState);

            //update graph json model
            getGraph().get(String.valueOf(memberId)).replace(String.valueOf(interactionId), outcome);
            log.info("Adjacency has been updated");
            return outcome;
        } finally {
            graphLock.unlock();
        }

    }

    @Override
    @NotNull
    public Optional<Person> createNewPerson(@NotNull final String name,
                                            @NotNull final Function<Long, String> pictureNameProducer) {
        if(peopleRepository.findByName(name) != null) {
            //person already exists
            log.error("Person '" + name + "' already exists");
            return Optional.empty();
        }

        final PersonImpl person = new PersonImpl(name, null);
        peopleRepository.saveAndFlush(person);
        person.setPicture(pictureNameProducer.apply(person.getId()));
        peopleRepository.saveAndFlush(person);

        return Optional.of(person);
    }

    @Override
    public void deletePerson(@NotNull final Person person) {
        peopleRepository.delete(person.getId());
        log.info("Person '" + person.getName() + "' has been deleted");
    }

    @Override
    public void applyPersonCreation(@NotNull final Person person, @NotNull final List<Long> initialInteractions) {
        peopleJson.add(new JsonPerson(person));

        final Map<String, Boolean> personMap = new HashMap<>();
        for(SocialInteraction interaction : socialInteractionsRepository.findAll()) {
            personMap.put(
                    String.valueOf(
                            interaction.getId()
                    ),
                    initialInteractions.stream()
                            .anyMatch(i -> i.equals(interaction.getId()))
            );
        }
        graphJson.put(String.valueOf(person.getId()), personMap);

        //create relationships entities
        for(Long id : initialInteractions) {
            relationshipManager.updateRelationship(id, person.getId(), true);
        }

        log.info("Model for person '" + person.getName() + "' has been updated");
    }

    @Override
    public boolean updateSocialInteractionsHierarchy(final long interactionId, final long parentId, final long groupId) {
        try {
            lockAll();

            SocialInteractionImpl interaction = socialInteractionsRepository.findById(interactionId);
            if(interaction == null){
                return false;
            }

            if(parentId == NO_PARENT) {
                boolean interactionUpdated = false;
                //remove parent
                if(interaction.getParentInteraction() != null) {
                    SocialInteractionImpl parent = (SocialInteractionImpl) interaction.getParentInteraction();
                    parent.getSubInteractions().remove(interaction);
                    interaction.setParentInteraction(null);
                    socialInteractionsRepository.save(parent);
                    interactionUpdated = true;
                }
                //set new group
                if(interaction.getGroup() == null || interaction.getGroup().getId() != groupId){
                    if(interaction.getGroup() != null){
                        //remove old group
                        SocialInteractionGroupImpl oldGroup = (SocialInteractionGroupImpl)interaction.getGroup();
                        oldGroup.getSocialInteractions().remove(interaction);
                        groupsRepository.save(oldGroup);
                    }

                    SocialInteractionGroupImpl group = groupsRepository.findById(groupId);
                    if(group == null){
                        return false;
                    }
                    interaction.setGroup(group);
                    groupsRepository.saveAndFlush(group);
                    interactionUpdated = true;
                }

                if(interactionUpdated) {
                    socialInteractionsRepository.saveAndFlush(interaction);
                    //cached model must be recreated
                    interactionsGroupsJson = null;
                    interactionsJson = null;
                }
                return true;
            } else {
                //prevent cycle creation
                if(isCyclePresent(interaction.getSubInteractions(), parentId)){
                    return false;
                }
                //remove from group
                if(interaction.getGroup() != null) {
                    SocialInteractionGroupImpl oldGroup = (SocialInteractionGroupImpl)interaction.getGroup();
                    oldGroup.getSocialInteractions().remove(interaction);
                    interaction.setGroup(null);
                    groupsRepository.saveAndFlush(oldGroup);
                }
                //remove old parent
                if(interaction.getParentInteraction() != null) {
                    SocialInteractionImpl oldParent = (SocialInteractionImpl) interaction.getParentInteraction();
                    oldParent.getSubInteractions().remove(interaction);
                    interaction.setParentInteraction(null);
                    socialInteractionsRepository.save(oldParent);
                }
                //set new parent
                SocialInteractionImpl parent = socialInteractionsRepository.findById(parentId);
                if(parent == null) {
                    return false;
                }
                parent.addSubInteraction(interaction);
                interaction.setParentInteraction(parent);
                socialInteractionsRepository.save(parent);
                socialInteractionsRepository.saveAndFlush(interaction);
                //cached model must be recreated
                interactionsGroupsJson = null;
                interactionsJson = null;

                return true;
            }
        } finally {
            unlockAll();
        }
    }

    @Override
    public boolean groupExists(@NotNull final String groupName) {
        try {
            groupsLock.lock();
            return !groupsRepository.findAllByName(groupName).isEmpty();
        } finally {
            groupsLock.unlock();
        }
    }

    @Override
    public boolean addGroup(@NotNull final String groupName) {
        try {
            lockAll();

            if(groupExists(groupName)) {
                return false;
            }

            SocialInteractionGroupImpl group = new SocialInteractionGroupImpl(groupName);
            groupsRepository.saveAndFlush(group);

            interactionsGroupsJson.add(new JsonInteractionGroup(group));

            return true;
        } finally {
            unlockAll();
        }
    }

    private boolean isCyclePresent(final Set<SocialInteraction> subInteractions, final long parentId) {
        if(subInteractions == null)
            return false;
        for(SocialInteraction subInteraction : subInteractions) {
            if(subInteraction.getId() == parentId) {
                return true;
            }
            if(isCyclePresent(subInteraction.getSubInteractions(), parentId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void recomputePositions() {
        try {
            lockAll();
            seAlgorithm.computePositions();
            log.info("SE algorithm has been rerun");
        } finally {
            unlockAll();
        }
    }

    @Override
    public boolean interactionExists(String name) {
        try {
            interactionLock.lock();
            return !socialInteractionsRepository.findAllByName(name).isEmpty();
        } finally {
            interactionLock.unlock();
        }
    }

    @Override
    public boolean addInteraction(@NotNull final String name, final long groupId,
                                  final long parentId, @NotNull final List<Long> people) {
        try {
            lockAll();

            if(interactionExists(name)) {
                return false;
            }

            final SocialInteractionImpl interaction = new SocialInteractionImpl(name);
            if(parentId == NO_PARENT) {
                //set group only
                final SocialInteractionGroupImpl group = groupsRepository.findById(groupId);
                if(group == null){
                    return false;
                }
                interaction.setGroup(group);
            } else {
                //set parent only
                final SocialInteractionImpl parent = socialInteractionsRepository.findById(parentId);
                if(parent == null) {
                    return false;
                }
                interaction.setParentInteraction(parent);
            }
            socialInteractionsRepository.saveAndFlush(interaction);

            try {
                textImageGenerator.generate(StringSplitter.splitString(interaction.getName()), String.valueOf(interaction.getId()));
            } catch (ImageGenerationException e) {
                socialInteractionsRepository.delete(interaction.getId());
                log.error("Couldn't create image for: '" + interaction.getName() + "'. Failing adding interaction");
                return false;
            }

            //create relationships
            for(Long personId : people) {
                relationshipManager.updateRelationship(interaction.getId(), personId, true);
            }

            recomputePositions();

            //cached model must be recreated
            interactionsGroupsJson = null;
            interactionsJson = null;
            graphJson = null;

            return true;
        } finally {
            unlockAll();
        }
    }

    @Override
    public void lockAll() {
        graphLock.lock();
        interactionLock.lock();
        allInteractionsLock.lock();
        groupsLock.lock();
        peopleLock.lock();
    }

    @Override
    public void unlockAll() {
        graphLock.unlock();
        interactionLock.unlock();
        allInteractionsLock.unlock();
        groupsLock.unlock();
        peopleLock.unlock();
    }
}
