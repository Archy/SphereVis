package com.sphereviz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class SphereVizApplication {

	public static void main(String[] args) {
		SpringApplication.run(SphereVizApplication.class, args);
	}
}
