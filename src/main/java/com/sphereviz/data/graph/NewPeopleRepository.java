package com.sphereviz.data.graph;

import com.sphereviz.model.graph.Person;
import com.sphereviz.model.graph.PersonImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link PersonImpl} repository
 */
@Repository
public interface NewPeopleRepository extends JpaRepository<PersonImpl, Long> {
    PersonImpl findByName(String name);

    PersonImpl findById(long id);

    /**
     *
     * @return list of all {@link Person} who don't have {@link com.sphereviz.model.users.User} assigned
     */
    @Query("SELECT p FROM PersonImpl p LEFT JOIN p.user AS u WHERE u.person=NULL")
    List<Person> findAllUnassigned();
}
