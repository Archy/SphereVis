package com.sphereviz.model.relationship;

/**
 * Entity representing relationship between {@link com.sphereviz.model.graph.Person}
 * and {@link com.sphereviz.model.users.User}
 */
public interface Relationship {

    long getId();

    long getInteractionId();

    long getPersonId();
}
