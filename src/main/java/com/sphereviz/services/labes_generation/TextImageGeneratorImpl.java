package com.sphereviz.services.labes_generation;

import com.sphereviz.utils.ImmutableMultiLineString;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Component
public class TextImageGeneratorImpl implements TextImageGenerator {
    private static final Logger log = Logger.getLogger(TextImageGeneratorImpl.class);

    private static final Font FONT = new Font("SansSerif", Font.BOLD, 48);
    private static final Color FONT_COLOR = new Color(0.18f, 0.04f, 0.33f);
    private static final Integer HIGHLIGHT_PIXEL_SIZE = 3;
    private static final Color HIGHLIGHT_COLOR = new Color(1f, 0f, 0f);
    private static final Integer NON_HIGHLIGHTED_PIXEL_SIZE = 1;
    private static final Color NON_HIGHLIGHTED_COLOR = new Color(1f, 1f, 1f);
    private static final Integer BORDER_PIXEL_SIZE = Integer.max(NON_HIGHLIGHTED_PIXEL_SIZE, HIGHLIGHT_PIXEL_SIZE);

    private static final String HIGHLITED_FILE_PREFIX = "highlighted";

    public void generate(@NotNull final ImmutableMultiLineString text,
                         @NotNull final String fileName) throws ImageGenerationException {
        try {
            generateTextImage(text, FONT, FONT_COLOR, NON_HIGHLIGHTED_COLOR, BORDER_PIXEL_SIZE, NON_HIGHLIGHTED_PIXEL_SIZE, fileName);
        } catch (IOException e) {
            throw  new ImageGenerationException("Image creation for: '" + text + "' failed", e);
        }
        try {
            generateTextImage(text, FONT, FONT_COLOR, HIGHLIGHT_COLOR, BORDER_PIXEL_SIZE, HIGHLIGHT_PIXEL_SIZE, HIGHLITED_FILE_PREFIX+"."+fileName);
        } catch (IOException e) {
            //remove created regular image
            File file = new File("photos/labels/"+fileName+".png");
            if(!file.delete()) {
                log.error("Created highlighted image for: '" + text + "' failed and removing regular image also failed");
            }
            throw  new ImageGenerationException("Image creation for: '" + text + "' failed", e);
        }

        log.info("Images for: '" + text.toString() + "' created and saved in '" + fileName + "'");
    }

    private void generateTextImage(final ImmutableMultiLineString text, final Font font, final Color fontColor, final Color highlightColor,
                                   final Integer borderPixelSize, final Integer highlightPixelSize, final String fileName) throws IOException{

        BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        int width = 0;

        for(String line : text.getLines()) {
            int currWidth =  fm.stringWidth(line) + 2*borderPixelSize;
            if(currWidth > width) {
                width = currWidth;
            }
        }

        int height = (fm.getHeight() + 2*borderPixelSize) * text.getLinesCount();
        g2d.dispose();

        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g2d = img.createGraphics();
        setRenderingHints(g2d);

        g2d.setFont(font);
        fm = g2d.getFontMetrics();

        //outside Font
        g2d.setColor(highlightColor);
        int y = 0;
        for (String line : text.getLines()) {
            int deltaWidth = (width - fm.stringWidth(line)) / 2;

            for (int i=borderPixelSize-highlightPixelSize; i<2*highlightPixelSize+1; i++){
                for (int j=borderPixelSize-highlightPixelSize; j<2*highlightPixelSize+1; j++){
                    g2d.drawString(line, i + deltaWidth, fm.getAscent() + j + y);
                }
            }
            y += fm.getHeight();
        }

        //inside Font
        g2d.setColor(fontColor);
        y = 0;
        for (String line : text.getLines()) {
            int deltaWidth = (width - fm.stringWidth(line)) / 2;
            g2d.drawString(line, borderPixelSize + deltaWidth, fm.getAscent() + borderPixelSize + y);
            y += fm.getHeight();
        }
        g2d.dispose();

        File file = new File("photos/labels/"+fileName+".png");
        ImageIO.write(img, "png", file);
    }

    private void setRenderingHints(Graphics2D g2d) {
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    }

}
