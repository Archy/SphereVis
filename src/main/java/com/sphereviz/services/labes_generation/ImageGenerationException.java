package com.sphereviz.services.labes_generation;

import java.io.IOException;

/**
 * Exception thrown when {@link TextImageGenerator} fails
 */
public class ImageGenerationException extends Exception {
    public ImageGenerationException(final String message, IOException reason) {
        super(message, reason);
    }
}
