package com.sphereviz.model.graph;

import com.sphereviz.model.users.User;
import com.sphereviz.model.users.UserImpl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.vecmath.Vector3d;


@Entity
@Table(
        name = "PERSON"
)
public class PersonImpl implements Person {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;
    @Column(name = "PICTURE", unique = true)
    private String picture;
    @Column(name = "POSITION")
    private Vector3d position;
    @OneToOne(targetEntity = UserImpl.class, mappedBy = "person")
    private User user;


    public PersonImpl() {
    }

    public PersonImpl(String name, String picture) {
        this.name = name;
        this.picture = picture;
    }

    public PersonImpl(String name, String picture, Vector3d position, User user) {
        this.name = name;
        this.picture = picture;
        this.position = position;
        this.user = user;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public Vector3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3d position) {
        this.position = position;
    }

    @Override
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
