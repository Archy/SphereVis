angular.module('socialViz')
    .controller('ngPersonChooseController', function ($route, $http, $routeParams, $templateCache) {
    });

angular.module('socialViz')
    .directive('ngRestCheckbox', function ($http, $location, $rootScope, syncSRV) {
        return {
            restrict: 'E',
            replace: true,
            template:
            "<div>" +
            "<input type='checkbox' ng-model='connected' ng-change='changeConnection()' ng-hide='updating' />" +
            "<img src='/images/spinner.gif' style='height: 35px; width: 35px;' ng-show='updating' />" +
            "</div>",
            scope: {
                interaction: '@interaction',
                member: '@member',
                token: "@token",
                checked: "@checked"
            },
            controller: ['$scope', function restCheckboxController($scope) {
                $scope.updating = false;
                
                $scope.changeConnection = function () {
                    if($scope.updating === false){
                        $scope.updating = true;

                        $http({
                            method: 'POST',
                            url: $location.protocol()+'://'+location.host+'/adjacency/rest',
                            data: {
                                interaction: $scope.interaction,
                                member: $scope.member,
                                checked: $scope.connected
                            },
                            headers: {
                                'X-CSRF-TOKEN': $scope.token
                            }
                        }).then(
                            function success(success) {
                                $scope.connected = success.data;
                                $scope.updating = false;

                                syncSRV.adjacencyNeedsReloadSrv();
                            },
                            function error(error) {
                                console.error("Adjacency update error");
                                $scope.connected = !$scope.connected;
                                $scope.updating = false;
                            }
                        );
                    } else {
                        $scope.connected = !$scope.connected;
                    }
                }
            }],
            link: function link(scope, element, attrs) {
                scope.$watch('$viewContentLoaded', function () {
                    scope.connected = scope.checked === "checked";
                });
            }
        };
    });

angular.module('socialViz')
    .directive('ngPersonChoose', function ($http, $compile, $route, $location, $routeParams, syncSRV, $templateCache, $q) {
        return {
            restrict: 'E',
            replace: false,
            template:
            "<div><span>" +
                "Loading..." +
                "<img src='/images/spinner.gif' style='height: 100px; width: 100px;'/>" +
            "</span></div>",
            scope: {
                leftHeight: '@leftHeight',
                rightHeight: '@rightHeight',
                token: '@token'
            },
            controller: ['$scope', function ngChoosePersonController($scope) {
                $scope.choosenPerson = {
                    model : null
                };

                $scope.addedPerson = {
                    name : null,
                    picture : null
                };

                $scope.choosing = false;
                $scope.adding = false;
            }],
            link: function link(scope, element, attrs) {
                var pageTemplate =
                    "<div class='row'>" +
                        "<div class='col-sm-6 col-md-6 col-lg-6'><div class='bordered-col'>" +
                            "<input type='search' ng-model='peopleFilter' placeholder='Filter' class='filter form-control'/>"+
                            "<form ng-submit='onFormSubmit()' name='chooseForm'>" +
                                "<div class='peopleList' style='height: {{leftHeight}} !important;'>" +
                                    "<div ng-repeat='person in people | filter : peopleFilter' class='radio'>" +
                                        "<label>" +
                                            "<input ng-value='{{person}}' ng-model='choosenPerson.model' type='radio' name='choosenPersonRadio' required/>" +
                                            "<span ng-bind='person.name'></span>" +
                                        "</label>" +
                                    "</div>" +
                                "</div>" +
                                "<button type='submit' class='btn btn-default'>Assign</button>" +
                                "<img src='/images/spinner.gif' style='height: 100px; width: 100px;' ng-show='choosing'/>" +
                            "</form>" +
                        "</div></div>" +
                        "<div class='clearfix visible-xs'></div>" +
                        "<div class='col-sm-6 col-md-6 col-lg-6 bordered'><div class='bordered-col'>" +
                            "<form ng-submit='onPersonAdd()' name='addForm' class='form-horizontal'>" +
                                "<div class='form-group'>" +
                                    "<label class='control-label col-sm-3 col-md-3 col-lg-3' for='personName'>Name:</label>" +
                                    "<div class='col-sm-9 col-md-9 col-lg-9'>" +
                                        "<input type='text' id='personName' name='personName' ng-model='addedPerson.name' required />" +
                                    "</div>" +
                                "</div>" +
                                "<div class='form-group'>" +
                                    "<label class='control-label col-sm-3 col-md-3 col-lg-3' for='portrait'>Portrait:</label>" +
                                    "<div class='col-sm-9 col-md-9 col-lg-9'>" +
                                        "<input type='file' id='portrait' name='portrait'  onchange='angular.element(this).scope().chooseFile(this.files)' required/>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='form-group'>" +
                                    "<div class='col-sm-12 col-md-12 col-lg-12'>" +
                                        "<label>" +
                                            "Connections:" +
                                            "<ng-clearable-search filter='interactionsFilter'></ng-clearable-search>" +
                                        "</label>" +
                                        "<div class='interactionList' style='height: {{rightHeight}} !important;'>" +
                                            "<div ng-repeat='interaction in interactions | filter : interactionsFilter'>" +
                                                "<label>" +
                                                    "<input ng-model='chosenInteractions[interaction.index]' ng-true-value='{{interaction.id}}' ng-false-value='null' type='checkbox'/>" +
                                                    "<span ng-bind='interaction.name'></span>" +
                                                "</label>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                "<button type='submit' class='btn btn-default'>Add and assign</button>" +
                                "<img src='/images/spinner.gif' style='height: 100px; width: 100px;' ng-show='adding'/>" +
                            "</form>" +
                        "</div></div>" +
                        "<div>" +
                    "</div>"
                ;

                function loadChooseForm(message) {
                    var contentPromises = [];

                    contentPromises.push(
                        $http.get('/adjacency/unassigned')
                            .then(
                                function success(success) {
                                    scope.people = success.data;
                                },
                                function error(error) {
                                    console.error("FAILED TO load unassigned people: " + error);
                                }
                            )
                    );

                    contentPromises.push(
                        $http.get('/rest/all-interactions.json')
                            .then(
                                function success(success) {
                                    scope.interactions = success.data;
                                    for(var i=0; i<scope.interactions.length; ++i) {
                                        scope.interactions[i].index = i;
                                    }

                                    scope.chosenInteractions = new Array(scope.interactions.length).fill(null);
                                },
                                function error(error) {
                                    console.error("FAILED TO load social interactions: " + error);
                                }
                            )
                    );

                    $q.all(contentPromises)
                        .then(
                            function success(success) {

                                var form = angular.element(
                                    "<div>" +
                                    "<h4>" + message + "</h4>" +
                                    pageTemplate
                                );
                                element.children().first().replaceWith(form);
                                $compile(element.contents())(scope);
                            },
                            function error(error) {
                                console.error("FAILED TO render page: " + error);
                            }
                        );
                }

                scope.$watch('$viewContentLoaded', function (){
                    loadChooseForm("Assign existing person to your account or add a new one");
                });

                scope.onFormSubmit = function () {
                    if(scope.chooseForm.$valid && !scope.adding) {
                        scope.choosing = true;

                        $http({
                            method: 'POST',
                            url: $location.protocol()+'://'+location.host+'/adjacency/assign',
                            data: scope.choosenPerson.model,
                            headers: {
                                'X-CSRF-TOKEN': scope.token
                            }
                        }).then(
                            function success(success) {
                                console.log("Person assigned successfully");
                                //reload

                                var currentPageTemplate = $route.current.templateUrl;
                                $templateCache.remove(currentPageTemplate);
                                $route.reload();
                            },
                            function error(error) {
                                loadChooseForm("Error assigning person, please try again");
                            }
                        );
                    }
                };

                scope.onPersonAdd = function () {
                    console.warn(scope.addForm.$valid + " " + scope.choosing);
                    if(!scope.choosing) {
                        scope.adding = true;

                        var fd = new FormData();
                        fd.append("file", scope.addedPerson.picture);
                        fd.append("name", scope.addedPerson.name)
                        fd.append("interactions", scope.chosenInteractions);

                        $http({
                            method: 'POST',
                            url: $location.protocol()+'://'+location.host+'/adjacency/person',
                            data: fd,
                            headers: {
                                'X-CSRF-TOKEN': scope.token,
                                'Content-Type': undefined
                            }
                        }).then(
                            function success(success) {
                                console.log("Person assigned successfully");
                                //reload
                                var currentPageTemplate = $route.current.templateUrl;
                                $templateCache.remove(currentPageTemplate);
                                $route.reload();
                            },
                            function error(error) {
                                $http.get('/adjacency/unassigned')
                                    .then(
                                        function success(success) {
                                            scope.choosing = false;
                                            scope.people = success.data;

                                            var form = angular.element(
                                                "<div>" +
                                                "<h4>Error adding person, please try again</h4>" +
                                                pageTemplate
                                            );
                                            element.children().first().replaceWith(form);
                                            $compile(element.contents())(scope);
                                        },
                                        function error(error) {
                                            console.error("FAILED TO load unassigned people: " + error);
                                        }
                                    );
                            }
                        );
                    }
                };

                scope.chooseFile = function(files) {
                    scope.addedPerson.picture = files[0];
                };

            }
        };
    });

angular.module('socialViz')
    .directive('ngEditAdjacency', function ($http, $compile, $location, $rootScope, syncSRV) {
        return {
            restrict: 'E',
            replace: true,
            template:
            "<div><span>" +
                "Loading..." +
                "<img src='/images/spinner.gif' style='height: 100px; width: 100px;'/>" +
            "</span></div>",
            scope: {
                token: "@token",
                personId: "@"
            },
            controller: ['$scope', function ($scope) {
                $scope.interactionsFilter = "";
            }],
            link: function link(scope, element, attrs) {
                var pageTemplate =
                    "<div>" +
                        "<ng-clearable-search filter='interactionsFilter'></ng-clearable-search>" +
                        "<div class='interactionList'>" +
                            "<table class='table' id='userTable'>" +
                                "<tr ng-repeat='connection in adjacencyModel | filter : interactionsFilter'>" +
                                    "<td style='left: 0; position: relative;' ng-bind='connection.interactionName'>" +
                                    "</td>" +
                                    "<th>" +
                                        "<ng-rest-checkbox " +
                                            "interaction='{{connection.interactionID}}' " +
                                            "member='{{personId}}' " +
                                            "token='{{token}}' " +
                                            "checked='{{connection.connected}}' >" +
                                        "</ng-rest-checkbox>" +
                                    "</th>" +
                                "</tr>" +
                            "</table>" +
                        "</div>" +
                    "</div>";

                scope.$watch('$viewContentLoaded', function () {
                    $http({
                        method: 'POST',
                        url: $location.protocol() + '://' + location.host + '/adjacency/model',
                        headers: {
                            'X-CSRF-TOKEN': scope.token,
                            'Content-Type': undefined
                        }
                    }).then(
                        function (model) {
                            scope.adjacencyModel = model.data;
                            console.warn(model.data);

                            for(var i=0; i<scope.adjacencyModel.length; ++i) {
                                var connected = scope.adjacencyModel[i].connected;
                                if(connected) {
                                    scope.adjacencyModel[i].connected = "checked";
                                } else {
                                    scope.adjacencyModel[i].connected = "false";
                                }
                            }

                            var form = angular.element(pageTemplate);
                            element.children().first().replaceWith(form);
                            $compile(element.contents())(scope);
                        },
                        function (model) {
                            console.error("Failed to load adjacency model");
                        }
                    );
                });
            }
        };
    });
