package com.sphereviz.controllers.interactions_management;

import com.sphereviz.services.graph.GraphManager;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Rest controller handling social interactions management
 */
@RestController
public class InteractionsManagementREST {
    private static final Logger log = Logger.getLogger(InteractionsManagementREST.class);

    private final GraphManager graphManager;


    @Inject
    public InteractionsManagementREST(@NotNull final GraphManager graphManager) {
        this.graphManager = graphManager;
    }

    @PostMapping("/interactions/rest/hierarchy")
    public boolean updateHierarchy(@RequestParam("interaction") final long interactionId,
                                   @RequestParam("group") final long groupId,
                                   @RequestParam("parent") final long parentId) {

        return graphManager.updateSocialInteractionsHierarchy(interactionId, parentId, groupId);
    }

    @PostMapping("/interactions/rest/unique-group")
    public boolean isGroupUnique(@RequestParam("group") final String name) {
        return !graphManager.groupExists(name);
    }

    @PostMapping("/interactions/rest/add-group")
    public boolean addGroup(@RequestParam("group") final String name) {
        return graphManager.addGroup(name);
    }

    @PostMapping("/interactions/rest/unique-interaction")
    public boolean isInteractionUnique(@RequestParam("name") final String name) {
        return !graphManager.interactionExists(name);
    }

    @PostMapping("/interactions/rest/add-interaction")
    public boolean addInteraction(@RequestParam("name") final String name,
                                  @RequestParam("group") final long group,
                                  @RequestParam("parent") final long parent,
                                  @RequestParam("people") final List<Long> people) {
        return graphManager.addInteraction(name, group, parent,
                people.stream().filter(Objects::nonNull).collect(Collectors.toList()));
    }
}
