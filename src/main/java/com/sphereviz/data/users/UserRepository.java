package com.sphereviz.data.users;

import com.sphereviz.model.users.UserImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * {@link UserImpl} repository
 */
@Repository
public interface UserRepository extends JpaRepository<UserImpl, Long> {
    UserImpl findUserByEmail(String email);

    /**
     *
     * @param personID {@link com.sphereviz.model.graph.Person} id
     * @return {@link UserImpl} who have assigned person with given id
     */
    @Query("SELECT u FROM UserImpl u LEFT JOIN u.person AS p WHERE u.person=p AND p.id = :person_id")
    UserImpl findWithPerson(@Param("person_id") long personID);
}
