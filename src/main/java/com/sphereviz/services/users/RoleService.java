package com.sphereviz.services.users;

import com.sphereviz.model.users.Role;
import com.sphereviz.model.users.RoleEnum;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Service, that handles finding user roles listed in {@link RoleEnum}
 */
public interface RoleService {
    /**
     *
     * @param roles roles to be found
     * @return list of {@link Role} entities representing users roles
     */
    @NotNull
    List<Role> findAllByName(RoleEnum ... roles);

    /**
     *
     * @param role role to be found
     * @return single {@link Role}, if provided role was found, null otherwise
     */
    @Nullable
    Role findByName(RoleEnum role);
}
