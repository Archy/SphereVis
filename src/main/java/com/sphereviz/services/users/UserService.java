package com.sphereviz.services.users;

import com.sphereviz.model.graph.Person;
import com.sphereviz.model.users.ConfirmationToken;
import com.sphereviz.model.users.User;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Service, that handles {@link User} management
 */
public interface UserService {
    /**
     * Creates admin user with admin/admin username/password
     */
    void createAdmin();

    /**
     * Checks if email is unique
     * @param email
     * @return true if provided email is not in database
     */
    boolean isEmailUnique(@NotNull final String email);

    /**
     * Adds new {@link User} to database
     * @param email user email
     * @param password user password, that is encoded firs
     * @return {@link com.sphereviz.model.users.UserImpl} instance if user was added successfully, null otherwise
     */
    @NotNull
    Optional<User> addUser(@NotNull final String email, @NotNull final String password);

    /**
     * Creates confirmation token for user.
     * Token is in fact confirmation link
     * @param user user for which token will be created
     * @return {@link ConfirmationToken} for user
     */
    @NotNull
    ConfirmationToken createConfirmationToken(@NotNull User user);

    /**
     * Confirms user email(account) when user click confirmation link
     * @param token token that was used for verification link creation
     * @return true if confirmation was successful
     */
    boolean confirmUserEmail(@NotNull final String token);

    /**
     * Finds {@link Person} assigned to {@link User}
     * @param email user email
     * @return {@link Person} if user has assigned person, null otherwise
     */
    @Nullable
    Person getUserPerson(@NotNull final String email);

    /**
     * Finds {@link User} assigned to {@link Person}
     * @param personID person id
     * @return {@link User} if person has assigned user, null otherwise
     */
    @Nullable
    User getPersonUser(@NotNull long personID);

    /**
     * Creates bidirectional assignment between {@link Person} and {@link User}
     * @param email user email
     * @param personID person id
     * @return true, if assignment was successful
     */
    boolean assignPerson(@NotNull final String email, long personID);

    /**
     * Removes relationship between person and user, fails silently if relation haven't existed
     * @param email user email
     */
    void revertAssignment(@NotNull final String email);

    /**
     *
     * @return list of all {@link Person} who don't have {@link com.sphereviz.model.users.User} assigned
     */
    List<Person> findAllUnassigned();
}
