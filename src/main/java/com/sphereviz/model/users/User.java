package com.sphereviz.model.users;

import com.sphereviz.model.graph.Person;

import java.util.List;

/**
 * Represents User entity
 */
public interface User {
    /**
     *
     * @return user unique email
     */
    String getEmail();

    /**
     *
     * @return user encoded password
     */
    String getPassword();

    /**
     *
     * @return {@link RoleImpl} roles assigned to user
     */
    List<Role> getRoles();

    /**
     *
     * @return {@link Person} person self-assigned to user
     */
    Person getPerson();
}
