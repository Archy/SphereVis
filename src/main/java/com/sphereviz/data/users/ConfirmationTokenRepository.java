package com.sphereviz.data.users;

import com.sphereviz.model.users.ConfirmationTokenImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * {@link ConfirmationTokenImpl} repository
 */
@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationTokenImpl, Long> {
    /**
     *
     * @param checkedToken token to be checked
     * @return true if token already exists in database
     */
    @Query("SELECT COUNT(token)>0 FROM ConfirmationTokenImpl token WHERE token.token = :checked_token")
    boolean tokenExists(@Param("checked_token") String checkedToken);

    ConfirmationTokenImpl findByToken(String token);
}
