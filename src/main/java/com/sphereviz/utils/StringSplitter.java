package com.sphereviz.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * For creating {@link ImmutableMultiLineString} from String
 */
public class StringSplitter {
    private static int DEFAULT_MAX_LINE_WIDTH = 12;

    public static ImmutableMultiLineString splitString(final String input) {
        return splitString(input, DEFAULT_MAX_LINE_WIDTH);
    }

    public static ImmutableMultiLineString splitString(final String input, final int lineLenght) {
        List<String> lines = new ArrayList<>();
        int maxLenght = 0;

        List<String> words =  new ArrayList<>(Arrays.asList(input.split("[- ]")));
        while (!words.isEmpty()) {
            StringBuilder currWord = new StringBuilder(words.get(0));
            words.remove(0);
            while (!words.isEmpty() && currWord.length()+words.get(0).length() < lineLenght) {
                currWord.append(" ").append(words.get(0));
                words.remove(0);
            }
            lines.add(currWord.toString());
            if(currWord.length() > maxLenght){
                maxLenght = currWord.length();
            }
        }

        MultiLineString result = new MultiLineString(Collections.unmodifiableList(lines), maxLenght);
        return result;
    }
}
