package com.sphereviz.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Main (default) controller
 */
@Controller
public class IndexController {
    @RequestMapping("/")
    public String get() {
        return "index";
    }

    @RequestMapping("/home.html")
    public String getHome(){
        return "home";
    }
}
