package com.sphereviz.services.users;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.users.ConfirmationTokenRepository;
import com.sphereviz.data.users.UserRepository;
import com.sphereviz.model.graph.Person;
import com.sphereviz.model.users.ConfirmationToken;
import com.sphereviz.model.users.ConfirmationTokenImpl;
import com.sphereviz.model.users.RoleEnum;
import com.sphereviz.model.users.User;
import com.sphereviz.model.users.UserImpl;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = Logger.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final NewPeopleRepository peopleRepository;
    private final ConfirmationTokenRepository tokenRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;


    @Inject
    public UserServiceImpl(@NotNull final UserRepository userRepository,
                           @NotNull final NewPeopleRepository peopleRepository,
                           @NotNull final ConfirmationTokenRepository tokenRepository,
                           @NotNull final RoleService roleService,
                           @NotNull final PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.peopleRepository = peopleRepository;
        this.tokenRepository = tokenRepository;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void createAdmin(){
        if(userRepository.findUserByEmail("admin") == null){
            userRepository.saveAndFlush(new UserImpl(
                    "admin",
                    passwordEncoder.encode("admin"),
                    roleService.findAllByName(RoleEnum.ADMIN, RoleEnum.UNASSIGNED))
            );
            log.info("Admin account created");
        }
    }

    @Override
    public boolean isEmailUnique(@NotNull final String email) {
        return userRepository.findUserByEmail(email) == null;
    }

    @NotNull
    @Override
    public Optional<User> addUser(@NotNull final String email,@NotNull final String password) {
        if(EmailValidator.getInstance().isValid(email) && isEmailUnique(email)){
            final UserImpl newUser = new UserImpl(email, passwordEncoder.encode(password),
                    roleService.findAllByName(RoleEnum.UNCONFIRMED));
            userRepository.saveAndFlush(newUser);
            return Optional.of(newUser);
        }
        log.error("Email " + email + " either is already in db or is not valid");
        return Optional.empty();
    }

    @Override
    public ConfirmationToken createConfirmationToken(@NotNull final User user) {
        String token;
        do{
            token = UUID.randomUUID().toString();
        }while(tokenRepository.tokenExists(token));
        log.info("Confirmation token for '" + user.getEmail() + "' created");
       return tokenRepository.saveAndFlush(new ConfirmationTokenImpl(token, user));
    }

    @Override
    public boolean confirmUserEmail(@NotNull final String token) {
        final ConfirmationTokenImpl confirmationToken = tokenRepository.findByToken(token);
        if(confirmationToken == null)
            return false;

        final User user = confirmationToken.getUser();

        if(user instanceof UserImpl && user.getRoles().remove(roleService.findByName(RoleEnum.UNCONFIRMED))){
            userRepository.saveAndFlush((UserImpl) user);
            tokenRepository.delete(confirmationToken);
            log.info("'" + user.getEmail() + "' confirmed account");
            return true;
        } else {
            log.error("Token '" + token + "' is not assigned to any user");
            return false;
        }
    }

    @Nullable
    @Override
    public Person getUserPerson(@NotNull final String email) {
        final User user = userRepository.findUserByEmail(email);
        if(user == null){
            throw new IllegalArgumentException("UserImpl with email " + email + " doesn't exist.");
        }
        return user.getPerson();
    }

    @Nullable
    @Override
    public User getPersonUser(@NotNull final long personID) {
        return userRepository.findWithPerson(personID);
    }

    @Override
    public boolean assignPerson(@NotNull final String email, @NotNull final  long personID) {
        final UserImpl user = userRepository.findUserByEmail(email);
        final Person person = peopleRepository.findById(personID);

        user.setPerson(person);
        userRepository.saveAndFlush(user);

        log.info("Assignment between user(" + user.getEmail() + " and person(" + person.getName() + ") has been created");

        return true;
    }

    @Override
    public void revertAssignment(@NotNull final String email) {
        final UserImpl user = userRepository.findUserByEmail(email);

        user.setPerson(null);
        userRepository.saveAndFlush(user);

        log.info("Person assignment for user(" + user.getId() + " has been deleted");
    }

    @Override
    public List<Person> findAllUnassigned() {
        return peopleRepository.findAllUnassigned();
    }
}
