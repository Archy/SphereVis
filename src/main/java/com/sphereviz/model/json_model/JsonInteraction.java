package com.sphereviz.model.json_model;

import com.sphereviz.model.graph.SocialInteraction;

import javax.validation.constraints.NotNull;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

/**
 * Json representation of {@link com.sphereviz.model.graph.SocialInteraction}
 */
public class JsonInteraction {
    private String id;
    private String name;
    private Vector3d position;
    private List<JsonInteraction> subInteractions = null;


    public JsonInteraction() {}

    public JsonInteraction(SocialInteraction socialInteraction) {
        this.id = String.valueOf(socialInteraction.getId());
        this.name = socialInteraction.getName();
        this.position = socialInteraction.getPosition();

        if(socialInteraction.hasSubInteractions()) {
            subInteractions = new ArrayList<>();
            for(SocialInteraction subInteraction : socialInteraction.getSubInteractions()){
                subInteractions.add(new JsonInteraction(subInteraction));
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(long id) {
        this.id = Long.toString(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector3d getPosition() {
        return position;
    }

    public void setPosition(Vector3d position) {
        this.position = position;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<JsonInteraction> getSubInteractions() {
        return subInteractions;
    }

    public void setSubInteractions(List<JsonInteraction> subInteractions) {
        this.subInteractions = subInteractions;
    }

    public void addSubIneractions(@NotNull final JsonInteraction subInteraction) {
        if(subInteractions == null) {
            subInteractions = new ArrayList<>();
        }
        subInteractions.add(subInteraction);
    }

    public boolean hasSubInteractions() {
        return  subInteractions!= null && subInteractions.size()>0;
    }
}
