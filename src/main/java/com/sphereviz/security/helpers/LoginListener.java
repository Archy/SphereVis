package com.sphereviz.security.helpers;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * Login event listener
 */
@Component
public class LoginListener implements ApplicationListener<AbstractAuthenticationEvent> {
    private static final Logger log = Logger.getLogger(LoginListener.class);

    @Override
    public void onApplicationEvent(@NotNull final AbstractAuthenticationEvent event)
    {
        if (event instanceof InteractiveAuthenticationSuccessEvent || event instanceof AuthenticationSuccessEvent) {
            log.info(event.getAuthentication().getName() + " logged in");
        } else if (event instanceof AbstractAuthenticationFailureEvent) {
            log.info(event.getAuthentication().getName() + " tried to log in, but failed miserably");
        }
    }


}