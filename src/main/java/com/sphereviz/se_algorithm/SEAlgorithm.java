package com.sphereviz.se_algorithm;


public interface SEAlgorithm {
    /**
     * Using Spherical Embedding algorithm computes {@link com.sphereviz.model.graph.VisEntity} entities positions on spheres
     */
    void computePositions();
}
