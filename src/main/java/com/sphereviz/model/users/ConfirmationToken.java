package com.sphereviz.model.users;

/**
 * Created by Janek on 2017-09-08.
 *
 * For internal use only.
 * Shouldn't be exposed outside service/
 */
public interface ConfirmationToken {
    /**
     *
     * @return {@link User} to whom token is assigned
     */
    User getUser();

    /**
     *
     * @return token for confirmation link creation
     */
    String getToken();
}
