package com.sphereviz.model.users;

/**
 * Immutable representation of {@link RoleImpl}
 */
public interface Role {
    /**
     *
     * @return role name
     */
    String getName();
}
