package com.sphereviz.controllers.adjacency;

import com.sphereviz.model.graph.Person;
import com.sphereviz.model.helpers.AdjacencyModel;
import com.sphereviz.model.helpers.UpdateJson;
import com.sphereviz.model.json_model.JsonPerson;
import com.sphereviz.model.users.RoleEnum;
import com.sphereviz.services.graph.GraphManager;
import com.sphereviz.services.users.UserService;
import org.apache.log4j.Logger;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Rest controller, that handles adjacency editing and person to user assignment
 */
@RestController
public class AdjacencyREST {
    private static final Logger log = Logger.getLogger(AdjacencyREST.class);
    private static final Path ROOT_LOCATION = Paths.get("./photos/photos");

    private final TemplateEngine templateEngine;
    private final  UserService userService;
    private final  GraphManager graphManager;


    @Inject
    public AdjacencyREST(@NotNull final TemplateEngine templateEngine,
                         @NotNull final UserService userService,
                         @NotNull final GraphManager graphManager) {
        this.templateEngine = templateEngine;
        this.userService = userService;
        this.graphManager = graphManager;
    }

    @PostMapping("/adjacency/rest")
    public boolean updateAdjacency(@RequestBody final UpdateJson updateData){
        final UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(userDetails.getAuthorities().contains(new SimpleGrantedAuthority(RoleEnum.ADMIN.getName()))){
            templateEngine.clearTemplateCacheFor("adjacency/admin");
            return graphManager.updateAdjacency(updateData.getGroup(),updateData.getInteraction(),
                                                    updateData.getMember(), updateData.isChecked());
        } else {
            final Person person = userService.getUserPerson(userDetails.getUsername());
            if(person == null){
                //shouldn't be here -> return previous state
                log.warn("User '" + userDetails.getUsername() + "' tried to edit adjacency without having assigned person");
                return graphManager.getGraph().get(String.valueOf(updateData.getMember()))
                                                .get(String.valueOf(updateData.getInteraction()));
            } else {
                //edit own adjacency
                return graphManager.updateAdjacency(updateData.getGroup(),updateData.getInteraction(),
                        updateData.getMember(), updateData.isChecked());
            }
        }
    }

    @GetMapping("/adjacency/unassigned")
    public List<JsonPerson> getUnassigned(){
        return userService.findAllUnassigned().stream()
                .map(JsonPerson::new)
                .collect(Collectors.toList());
    }

    @PostMapping("/adjacency/assign")
    public boolean assigns(@RequestBody final JsonPerson jsonPerson){
        final UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(userService.getUserPerson(userDetails.getUsername()) == null &&
                userService.getPersonUser(Long.parseLong(jsonPerson.getID())) == null ){
            //assign jsonPerson to user
            return userService.assignPerson(userDetails.getUsername(), Long.parseLong(jsonPerson.getID()));
        } else {
            //user already have assigned jsonPerson
            log.warn("User '" + userDetails.getUsername() + "' tried to assign another person to his account");
            return false;
        }
    }

    @PostMapping("/adjacency/person")
    public boolean createNewPerson(@RequestParam("file") final MultipartFile file,
                                   @RequestParam("name") final String name,
                                   @RequestParam("interactions") final List<String> jsonConnections) {
        if(file==null || name==null)
            return false;

        final UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(userService.getUserPerson(userDetails.getUsername()) == null) {
            try {
                graphManager.lockAll();

                final String filename = StringUtils.cleanPath(file.getOriginalFilename());
                log.info("Person '" + name + "' with '" + filename + "' is under creation");

                if (file.isEmpty() || filename.contains("..")) {
                    //Cannot store empty file or file with relative path outside current directory
                    log.error("Cannot store empty file or file with relative path outside current directory");
                    return false;
                }
                //create person
                final Optional<Person> createdPerson = graphManager.createNewPerson(name, id -> "/photos/"+id);
                if(!createdPerson.isPresent()) {
                    //creation failed
                    log.error("Person creation failed");
                    return false;
                }
                //assign person
                if(!userService.assignPerson(userDetails.getUsername(), createdPerson.get().getId())) {
                    //assignment failed
                    graphManager.deletePerson(createdPerson.get());    //rollback
                    log.error("assignment failed");
                    return false;
                }

                try {
                    //save picture
                    Files.copy(file.getInputStream(),
                            ROOT_LOCATION.resolve(
                                    createdPerson.get().getPicture().substring("/photos/".length()) + ".jpg"
                            ),
                            StandardCopyOption.REPLACE_EXISTING);
                    graphManager.recomputePositions();
                    graphManager.applyPersonCreation(createdPerson.get(),
                            jsonConnections.stream()
                                    .filter(Objects::nonNull)
                                    .filter(((Predicate<String>) String::isEmpty).negate())
                                    .map(Long::parseLong)
                                    .collect(Collectors.toList())
                    );
                    return true;
                }
                catch (IOException e) {
                    //rollback
                    userService.revertAssignment(userDetails.getUsername());
                    graphManager.deletePerson(createdPerson.get());
                    log.error("saving picture failed: " + e);
                    return false;
                }
            } finally {
                graphManager.unlockAll();
            }
        } else {
            return false;
        }
    }

    @PostMapping("/adjacency/model")
    public List<AdjacencyModel> postModel() {
        final UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final Person person = userService.getUserPerson(userDetails.getUsername());
        final Map<String, Map<String, Boolean>> graph = graphManager.getGraph();

        if(person == null) {
            log.error("User with no person assigned tried to get adjacency model");
            return null;
        }

        return graphManager.getAllSocialInteractions().stream()
                .map(interaction -> new AdjacencyModel(
                        interaction.getId(), interaction.getName(),
                        graph.get(String.valueOf(person.getId()))
                                .get(String.valueOf(interaction.getId()))
                ))
                .collect(Collectors.toList());
    }
}
