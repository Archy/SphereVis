package com.sphereviz.model.graph;

import com.sphereviz.model.users.User;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import javax.vecmath.Vector3d;

/**
 * Entity representing person in social network
 */
public interface Person extends VisEntity {
    /**
     *
     * @return id
     */
    long getId();

    /**
     *
     * @return persons name + surname
     */
    @NotNull
    String getName();

    /**
     *
     * @return url to persons picture
     */
    @Nullable
    String getPicture();

    /**
     * Postion is null before {@link com.sphereviz.se_algorithm.SEAlgorithm run}
     *
     * @return portrait position on sphere
     */
    @Nullable
    Vector3d getPosition();

    /**
     *
     * @return assigned user (if any)
     */
    @Nullable
    User getUser();
}
