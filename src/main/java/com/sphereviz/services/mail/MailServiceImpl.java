package com.sphereviz.services.mail;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotNull;


@Component
@PropertySource("classpath:mail.properties")
public class MailServiceImpl implements MailService {
    private static final Logger log = Logger.getLogger(MailServiceImpl.class);

    private final JavaMailSender mailSender;

    @Value("${mail.from}")
    private String from;


    @Inject
    public MailServiceImpl(@NotNull final JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendMail(@NotNull final String to, @NotNull final String subject, @NotNull final String msg) {

        try {
            MimeMessage mail = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mail, true);
            messageHelper.setTo(to);
            messageHelper.setSubject("Confirmation email");
            messageHelper.setText(msg, true);
            mailSender.send(mail);
            log.info("Confirmation email to'" + to + "' has been send");
        } catch (MessagingException e) {
            log.error("Exception while sending confirmation email: " + e);
        }
    }
}
