package com.sphereviz.model.graph;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Entity representing group of {@link SocialInteraction}
 */
public interface SocialInteractionGroup {
    /**
     *
     * @return group id
     */
    long getId();

    /**
     *
     * @return group name
     */
    @NotNull
    String getName();

    /**
     *
     * @return list of all aggregated {@link SocialInteraction} by this group
     */
    @NotNull
    Set<SocialInteraction> getSocialInteractions();
}
