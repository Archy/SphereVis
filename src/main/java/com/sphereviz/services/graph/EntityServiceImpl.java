package com.sphereviz.services.graph;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.model.graph.PersonImpl;
import com.sphereviz.model.graph.SocialInteractionImpl;
import com.sphereviz.model.graph.VisEntity;
import com.sphereviz.services.relationship.RelationshipManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.concurrent.NotThreadSafe;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


@NotThreadSafe
@Service
public class EntityServiceImpl implements EntityService {
    private static final Logger log = Logger.getLogger(EntityServiceImpl.class);

    private final RelationshipManager relationshipManager;

    private final NewPeopleRepository peopleRepository;
    private final SocialInteractionsRepository socialInteractionsRepository;

    private List<PersonImpl> people;
    private List<SocialInteractionImpl> socialInteractions;


    @Inject
    public EntityServiceImpl(@NotNull final RelationshipManager relationshipManager,
                             @NotNull final NewPeopleRepository peopleRepository,
                             @NotNull final SocialInteractionsRepository socialInteractionsRepository) {
        this.relationshipManager = relationshipManager;
        this.peopleRepository = peopleRepository;
        this.socialInteractionsRepository = socialInteractionsRepository;
    }

    @Override
    public List<VisEntity> getAllEntities() {
        final List<VisEntity> list = new ArrayList<>();

        people = peopleRepository.findAll();
        list.addAll(people);
        socialInteractions = socialInteractionsRepository.findAll();
        list.addAll(socialInteractions);

        return list;
    }

    @Override
    public void safeAllEntitiesPositions() {
        peopleRepository.save(people);
        peopleRepository.flush();

        socialInteractionsRepository.save(socialInteractions);
    }

    @Override
    public <T extends VisEntity, S extends VisEntity> boolean areConnected(@NotNull T e1, @NotNull S e2) {
        return relationshipManager.relationshipExists(e1, e2);
    }
}
