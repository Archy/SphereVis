angular.module('socialViz')
    .directive('ngUniqueElement', function ($http, $location) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                token: '@',
                notUnique: '=',
                displayMessage: '=',
                checkingUniqueness: '=',
                url: '@',
                paramName: '@'
            },
            controller: ['$scope', function ngUniquenessController($scope) {
            }],
            link: function link(scope, element, attrs, ngModel) {
                element.bind('blur', function (e) {
                    if (!ngModel || !element.val()) return;
                    var currentValue = element.val();
                    scope.checkingUniqueness = true;

                    var fd = new FormData();
                    fd.append(scope.paramName , currentValue);

                    $http({
                        method: 'POST',
                        url: $location.protocol() + '://' + location.host + scope.url,
                        data: fd,
                        headers: {
                            'X-CSRF-TOKEN': scope.token,
                            'Content-Type': undefined
                        }
                    }).then(
                        function (unique) {
                            //Ensure value that being checked hasn't changed
                            if (currentValue === element.val() && unique.data === true) {
                                scope.notUnique = false;
                                scope.displayMessage = false;
                            } else {
                                scope.notUnique = true;
                                scope.displayMessage = true;
                            }
                            scope.checkingUniqueness = false;
                        },
                        function (unique) {
                            scope.notUnique = true;
                            scope.displayMessage = true;
                            scope.checkingUniqueness = false;
                        }
                    );
                });
            }
        };
    });

angular.module('socialViz').directive("ngClearableSearch", function () {
    return {
        restrict: 'E',
        replace: true,
        template:
        "<div class='filter'>" +
        "<input type='text' class='form-control-inline'  ng-model='filter' placeholder='Filter'/>" +
        "<a class='clear' ng-click='clearSearch()'>" +
        "<span class='glyphicon glyphicon-remove'></span>" +
        "</a>" +
        "</div>",
        scope: {
            filter: '=filter'
        },
        controller: ['$scope', function($scope) {
            $scope.clearSearch = function () {
                $scope.filter = "";
            };
        }],
        link: function link(scope, element, attrs) {
        }
    };
});
