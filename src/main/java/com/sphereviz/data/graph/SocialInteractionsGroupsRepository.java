package com.sphereviz.data.graph;

import com.sphereviz.model.graph.SocialInteractionGroupImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link SocialInteractionGroupImpl} repository
 */
@Repository
public interface SocialInteractionsGroupsRepository extends JpaRepository<SocialInteractionGroupImpl, Long> {
    SocialInteractionGroupImpl findById(long id);

    List<SocialInteractionGroupImpl> findAllByName(final String name);
}
