package com.sphereviz.data;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.data.relationship.RelationshipRepository;
import com.sphereviz.model.graph.PersonImpl;
import com.sphereviz.model.graph.SocialInteractionImpl;
import com.sphereviz.model.relationship.RelationshipImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Tests for {@link RelationshipRepository}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RelationshipRepositoryTest {
    @Autowired
    private RelationshipRepository relationshipRepository;
    @Autowired
    private NewPeopleRepository peopleRepository;
    @Autowired
    private SocialInteractionsRepository socialInteractionsRepository;

    private List<RelationshipImpl> relationshipList;

    @Before
    public void setUp() {
        relationshipList = relationshipRepository.findAll();
    }

    @Test
    public void testRelationExistsQuery() {
        for(RelationshipImpl relationship : relationshipList) {
            PersonImpl person = peopleRepository.findById(relationship.getPersonId());
            SocialInteractionImpl interaction = socialInteractionsRepository.findById(relationship.getInteractionId());

            assertThat(relationshipRepository.relationExists(person, interaction), is(true));
        }
    }

    @Test
    public void testFindByPersonIdAndInteractionIdQuery() {
        for(RelationshipImpl relationship : relationshipList) {
            RelationshipImpl loaded = relationshipRepository.findByPersonIdAndInteractionId(
                    relationship.getPersonId(), relationship.getInteractionId()
            );

            assertThat(loaded, is(notNullValue()));
            assertThat(loaded.getId(), equalTo(relationship.getId()));
        }
    }
}
