package com.sphereviz.security.helpers;

import com.sphereviz.data.users.UserRepository;
import com.sphereviz.model.users.Role;
import com.sphereviz.model.users.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Custom {@link UserDetailsService}, handling database specific user details storage
 */
@Component
public class SpVisUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;


    @Inject
    public SpVisUserDetailsService(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(@NotNull final String email) throws UsernameNotFoundException {
        final User user = userRepository.findUserByEmail(email);
        if(user == null)
            throw new UsernameNotFoundException("User not found");

        final Set<GrantedAuthority> grantedAuthority = new HashSet<>();
        grantedAuthority.add(new SimpleGrantedAuthority("admin"));

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(), convertAuthorities(user.getRoles())
        );
    }

    private Set<GrantedAuthority> convertAuthorities(List<Role> userRoles) {
        return userRoles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toSet());
    }
}