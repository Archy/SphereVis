package com.sphereviz.controllers.login;

import com.sphereviz.model.helpers.Message;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Login controller, that handler unconfirmed user and failed login attempt
 */
@Controller
public class LoginController {

    @GetMapping("/login/failure")
    public String failure(final Model model) {
        model.addAttribute("message",
                new Message("Wrong email and/or password",
                        "/", "Back to main page"));
        return "template";
    }

    @GetMapping("/login/unconfirmed")
    public String unconfirmed(){
        return "login_unconfirmed";
    }
}
