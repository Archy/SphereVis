package com.sphereviz.services;

import com.sphereviz.services.labes_generation.TextImageGenerator;
import com.sphereviz.services.labes_generation.TextImageGeneratorImpl;
import com.sphereviz.utils.StringSplitter;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(JUnitParamsRunner.class)
public class TextImageGeneratorTest {
    //cleanup, turn false if want to test image appearance.
    private static final Boolean CLEANUP = false;

    private static final String TEST_TEXT_PREFIX = "test-";
    private static final String PATH_STR_PREFIX = "photos/labels/test-";
    private static final String HIGHLIGHTED_PATH_STR_PREFIX = "photos/labels/highlighted.test-";

    private static final TextImageGenerator textImageGenerator = new TextImageGeneratorImpl();


    @Test
    @Parameters
    public void generateImageFile(final String testText, final String fileName, final String pathStr,
                                  final String highlightedPathStr, final int stringLenght) throws Exception{
        final File testFile = new File(pathStr);
        final File testHighlightedFile = new File(highlightedPathStr);
        textImageGenerator.generate(StringSplitter.splitString(testText, stringLenght), fileName);

        final BufferedImage img, img2;

        img = ImageIO.read(testFile);
        img2 = ImageIO.read(testHighlightedFile);

        assertThat(img, is(notNullValue()));
        assertThat(img2, is(notNullValue()));

        if (CLEANUP) {
            Files.deleteIfExists(testFile.toPath());
            Files.deleteIfExists(testHighlightedFile.toPath());
        }
    }

    @SuppressWarnings("unused")
    private Object[] parametersForGenerateImageFile() {
        return new Object[][]{
            {TEST_TEXT_PREFIX + "0001", TEST_TEXT_PREFIX + "0001",
                    PATH_STR_PREFIX+ "0001.png", HIGHLIGHTED_PATH_STR_PREFIX + "0001.png", 100},
            {TEST_TEXT_PREFIX + "0002", TEST_TEXT_PREFIX + "0002",
                    PATH_STR_PREFIX+ "0002.png", HIGHLIGHTED_PATH_STR_PREFIX + "0002.png", 4},
        };
    }
}
