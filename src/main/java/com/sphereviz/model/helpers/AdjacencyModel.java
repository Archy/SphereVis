package com.sphereviz.model.helpers;

/**
 * Model used in {@link com.sphereviz.controllers.adjacency.AdjacencyController}
 * for editing own adjacency by {@link com.sphereviz.model.users.User}
 */
public class AdjacencyModel {
    private long interactionID;
    private String interactionName;

    private boolean connected;


    public AdjacencyModel() {
    }

    public AdjacencyModel(long interactionID, String interactionName, boolean connected) {
        this.interactionID = interactionID;
        this.interactionName = interactionName;
        this.connected = connected;
    }

    public long getInteractionID() {
        return interactionID;
    }

    public void setInteractionID(long interactionID) {
        this.interactionID = interactionID;
    }

    public String getInteractionName() {
        return interactionName;
    }

    public void setInteractionName(String interactionName) {
        this.interactionName = interactionName;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
