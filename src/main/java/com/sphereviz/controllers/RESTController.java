package com.sphereviz.controllers;

import com.sphereviz.model.json_model.JsonInteraction;
import com.sphereviz.model.json_model.JsonInteractionGroup;
import com.sphereviz.model.json_model.JsonPerson;
import com.sphereviz.services.graph.GraphManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Sends visualization data in json format to client
 */
@RestController
public class RESTController {
    private final GraphManager graphManager;


    @Inject
    public RESTController(@NotNull final GraphManager graphManager) {
        this.graphManager = graphManager;
    }

    @RequestMapping("/rest/people.json")
    public List<JsonPerson> getPeople() {
        return graphManager.getPeople();
    }

    @RequestMapping("/rest/interactions.json")
    public List<JsonInteractionGroup> getInteractions() {
        return graphManager.getInteractionsGroups();
    }

    @RequestMapping("/rest/all-interactions.json")
    public List<JsonInteraction> getAllInteractions() {
        return graphManager.getInteractions();
    }

    @RequestMapping("/rest/graph.json")
    public Map<String, Map<String, Boolean>> getGraph() {
        return graphManager.getGraph();
    }
}