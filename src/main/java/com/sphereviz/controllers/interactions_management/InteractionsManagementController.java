package com.sphereviz.controllers.interactions_management;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller handling social interactions management
 */
@Controller
public class InteractionsManagementController {
    private static final Logger log = Logger.getLogger(InteractionsManagementController.class);


    @GetMapping("/interactions/hierarchy")
    public String manageHierarchy(){
        return "interactions/hierarchy";
    }

    @GetMapping("/interactions/add-interactions")
    public String addInteraction(){
        return "interactions/addInteraction";
    }

    @GetMapping("/interactions/add-group")
    public String addGroup(){
        return "interactions/addGroup";
    }
}
