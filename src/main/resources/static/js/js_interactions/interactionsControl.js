angular.module('socialViz')
    .directive('ngEditHierarchy', function ($http, $compile, $route, $location, $routeParams, syncSRV, $templateCache) {
        return {
            restrict: 'E',
            replace: false,
            template:
            "<div><span>" +
                "Loading..." +
                "<img src='/images/spinner.gif' style='height: 100px; width: 100px;'/>" +
            "</span></div>",
            scope: {
                token: '@token'
            },
            controller: ['$scope', function ngChoosePersonController($scope) {
                $scope.sending = false;
            }],
            link: function link(scope, element, attrs) {
                var pageTemplate =
                        "<form ng-submit='updateHierarchy()' class='form-horizontal' >" +
                            "<div class='form-group'>" +
                                "<label for='keywordSelect'>Select keyword:</label><br>" +
                                    "<select name='keywordSelect' id='keywordSelect' placeholder='Select keyword...'>" +
                                    "</select>" +
                            "</div>" +
                            "<div class='form-group' ng-if='selectedKeyword'>" +
                                "<label for='groupSelect'>Change group:</label><br>" +
                                "<select name='groupSelect' id='groupSelect'>" +
                                "</select>" +
                            "</div>" +
                            "<div class='form-group' ng-if='selectedKeyword'>" +
                                "<label for='parentSelect'>Change parent:</label><br>" +
                                "<select name='parentSelect' id='parentSelect'>" +
                                "</select>" +
                            "</div>" +
                            "<button type='submit' class='btn btn-default' ng-if='selectedKeyword' >Update hierarchy</button>" +
                            "<img src='/images/spinner.gif' style='height: 100px; width: 100px;' ng-show='sending'/>" +
                        "</form>" +
                    "</div>";

                var $keywordSelect = null;
                var $groupSelect = null;
                var $parentSelect = null;

                scope.updateHierarchy = function () {
                    if(!scope.sending && scope.selectedGroup != null && scope.selectedKeyword != null) {
                        console.log("updating hierarchy");
                        scope.sending = true;

                        var fd = new FormData();
                        fd.append("interaction", scope.selectedKeyword.id);
                        fd.append("group", scope.selectedGroup.id);
                        fd.append("parent", scope.selectedParent != null ? scope.selectedParent.id : -1);

                        $http({
                            method: 'POST',
                            url: $location.protocol() + '://' + location.host + '/interactions/rest/hierarchy',
                            data: fd,
                            headers: {
                                'X-CSRF-TOKEN': scope.token,
                                'Content-Type': undefined
                            }
                        }).then(
                            function success(success) {
                                scope.sending = false;
                                if(success.data) {
                                    scope.loadForm("Hierarchy updated successfully");
                                } else {
                                    scope.loadForm("An error has occurred while updating hierarchy.<br>Please try again");
                                }

                            },
                            function error(error) {
                                scope.sending = false;
                                scope.loadForm("An error has occurred while updating hierarchy.<br>Please try again");
                            }
                        );
                    }
                };

                scope.$watch('$viewContentLoaded', function (){
                    scope.loadForm("Edit keywords hierarchy");
                });

                scope.loadForm = function (message) {
                    $http.get('/rest/interactions.json')
                        .then(
                            function success(success) {
                                scope.selectedKeyword = null;
                                scope.selectedGroup = null;
                                scope.selectedGroupList = null;
                                scope.selectedParent = null;

                                scope.allInteractions = [];
                                scope.interactionByGroup = {};
                                scope.hierarchicalInteractions = success.data;

                                for (var i = 0; i < scope.hierarchicalInteractions.length; ++i) {
                                    scope.interactionByGroup[scope.hierarchicalInteractions[i].id] = [];

                                    scope.parseHierarchicalInteractions(scope.hierarchicalInteractions[i].types,
                                        scope.interactionByGroup[scope.hierarchicalInteractions[i].id]);
                                }

                                var form = angular.element(
                                    "<div>" +
                                    "<h4>" + message + "</h4>" +
                                    pageTemplate
                                );
                                element.children().first().replaceWith(form);
                                $compile(element.contents())(scope);

                                $keywordSelect = $('#keywordSelect').selectize({
                                    maxItems: 1,
                                    valueField: 'id',
                                    labelField: 'name',
                                    searchField: 'name',
                                    sortField: {field: 'name'},
                                    options: scope.allInteractions,
                                    create: false,
                                    onChange: scope.keywordSelectEventhandler()
                                });
                            },
                            function error(error) {
                                console.error("FAILED TO load social interactions: " + error);
                            }
                        );
                };
                
                scope.parseHierarchicalInteractions = function(interactions, groupList) {
                    for (var i = 0; i < interactions.length; ++i) {
                        scope.allInteractions.push(interactions[i]);
                        groupList.push(interactions[i]);

                        if(interactions[i].subInteractions !== null) {
                            scope.parseHierarchicalInteractions(interactions[i].subInteractions, groupList);
                        }
                    }
                };
                
                scope.keywordSelectEventhandler = function () {
                    return function() {
                        var id = arguments[0];
                        scope.selectedKeyword = null;

                        for(var i=0; i<scope.allInteractions.length; ++i) {
                            if(scope.allInteractions[i].id === id) {
                                scope.selectedKeyword = scope.allInteractions[i];
                                console.log(scope.selectedKeyword.name + " chosen");
                                break;
                            }
                        }
                        scope.$apply();

                        scope.selectedGroup = null;
                        scope.selectedGroupList = null;
                        scope.selectedParent = null;
                        for(var i=0; i<scope.hierarchicalInteractions.length && scope.selectedGroup === null; ++i) {
                            var groupList = scope.interactionByGroup[scope.hierarchicalInteractions[i].id];
                            for(var j=0; j<groupList.length && scope.selectedGroup === null; ++j) {
                                if(groupList[j].id === id) {
                                    scope.selectedGroup = scope.hierarchicalInteractions[i];
                                    scope.selectedGroupList = groupList;
                                }
                            }
                        }
                        if(scope.selectedGroup == null) {
                            scope.$apply();
                            return;
                        }

                        $groupSelect = $('#groupSelect').selectize({
                            maxItems: 1,
                            valueField: 'id',
                            labelField: 'name',
                            searchField: 'name',
                            sortField: {field: 'name'},
                            options: scope.hierarchicalInteractions,
                            items: [scope.selectedGroup.id],
                            create: false,
                            allowEmptyOption: false,
                            onChange: scope.groupSelectEventhandler()
                        });

                        scope.findParent(scope.selectedKeyword.id, scope.selectedGroupList);

                        $parentSelect = $('#parentSelect').selectize({
                            maxItems: 1,
                            valueField: 'id',
                            labelField: 'name',
                            searchField: 'name',
                            sortField: {field: 'name'},
                            options: scope.createPotentialParentsList(),
                            items: scope.selectedParent !== null ? [scope.selectedParent.id] : [],
                            create: false,
                            allowEmptyOption: true,
                            onChange: scope.parentSelectEventhandler()
                        });
                    };
                };

                scope.groupSelectEventhandler = function () {
                    return function () {
                        var id = arguments[0];
                        scope.selectedGroupList = scope.interactionByGroup[id];
                        for(var i=0; i < scope.hierarchicalInteractions.length; ++i) {
                            if(scope.hierarchicalInteractions[i].id === id) {
                                scope.selectedGroup = scope.hierarchicalInteractions[i];
                                break;
                            }
                        }
                        if(scope.selectedGroup == null) {
                            console.error("Selected group is null");
                        }

                        // fetch the instance
                        var selectize = $parentSelect[0].selectize;
                        //update potential parent list to choose from
                        selectize.clearOptions();
                        selectize.addOption(scope.createPotentialParentsList());
                        selectize.clear();
                    };
                };

                scope.parentSelectEventhandler = function () {
                    return function () {
                        var id = arguments[0];
                        scope.selectedParent = null;
                        for(var i=0; i<scope.selectedGroupList.length; ++i) {
                            if(scope.selectedGroupList[i].id === id) {
                                scope.selectedParent = scope.selectedGroupList[i];
                                break;
                            }
                        }
                    }
                };

                scope.findParent = function(childId, interactions) {
                    for(var i=0; i<interactions.length; ++i) {
                        var interaction = interactions[i];
                        if(interaction.subInteractions != null) {
                            for(var j=0; j<interaction.subInteractions.length; ++j) {
                                if(interaction.subInteractions[j].id === childId) {
                                    scope.selectedParent = interaction;
                                    return true;
                                }
                            }
                            if(scope.findParent(childId, interaction.subInteractions)) {
                                return true;
                            }
                        }
                    }
                    return false;
                };

                scope.createPotentialParentsList = function() {
                    //creates list with all in group but the sub interactions of chosen one
                    var allChildren = [];
                    var potentialParents = [];

                    allChildren.push(scope.selectedKeyword);
                    if(scope.selectedKeyword.subInteractions != null) {
                        scope.pushSubInteractionToList(scope.selectedKeyword.subInteractions, allChildren);
                    }

                    for(var i=0; i<scope.selectedGroupList.length; ++i) {
                        if($.inArray(scope.selectedGroupList[i], allChildren) === -1) {
                            potentialParents.push(scope.selectedGroupList[i]);
                        }
                    }
                    return potentialParents;
                };

                scope.pushSubInteractionToList = function (interactions, list) {
                    for(var i=0; i<interactions.length; ++i) {
                        list.push(interactions[i]);
                        if(interactions[i].subInteractions !== null) {
                            scope.pushSubInteractionToList(interactions[i].subInteractions, list);
                        }
                    }
                }
            }
        };
    });

angular.module('socialViz')
    .directive('ngAddGroup', function ($http, $compile, $route, $location, $routeParams, syncSRV, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            template:
            "<div>" +
                "<h4>Add new group</h4>" +
                "<div ng-if='resultMessage'><span ng-bind='resultMessage'></span></div>" +
                "<form ng-submit='addGroup()'>" +
                    "<div class='form-group'>" +
                        "<label for='groupName'>Group name:</label><br>" +
                        "<input ng-unique-element    " +
                                "token='{{token}}' " +
                                "not-unique='notUnique' " +
                                "display-message='displayMessage' " +
                                "checking-uniqueness='checkingUniqueness' " +
                                "url='/interactions/rest/unique-group'" +
                                "param-name='group'" +
                                "data-ng-model='groupName' " +
                                "required='required'>" +
                        "<img src='/images/spinner.gif' ng-show='checkingUniqueness' style='height: 50px; width: 50px;'/>" +
                        "<span class='glyphicon glyphicon-ok' class='tick' ng-show='notUnique===false'></span>" +
                        "<span ng-show='displayMessage'><br>Group already exists<br></span>" +
                    "</div>" +
                    "<button type='submit' class='btn btn-default' ng-disabled='notUnique'>" +
                        "Add" +
                    "</button>" +
                "</form>" +
            "</div>",
            scope: {
                token: '@token'
            },
            controller: ['$scope', function ngChoosePersonController($scope) {
                $scope.notUnique = true;
                $scope.displayMessage = false;
                $scope.checkingUniqueness = false;
                $scope.groupName = "";

                $scope.resultMessage = null;
            }],
            link: function link(scope, element, attrs) {
                scope.addGroup = function () {
                    var fd = new FormData();
                    fd.append("group", scope.groupName);

                    $http({
                        method: 'POST',
                        url: $location.protocol() + '://' + location.host + '/interactions/rest/add-group',
                        data: fd,
                        headers: {
                            'X-CSRF-TOKEN': scope.token,
                            'Content-Type': undefined
                        }
                    }).then(
                        function (unique) {
                            scope.notUnique = true;
                            scope.displayMessage = false;
                            scope.checkingUniqueness = false;
                            scope.groupName = "";

                            if(unique.data) {
                                scope.resultMessage = "Group added successfully";
                            } else {
                                scope.resultMessage = "An error has happened. Ply try again";
                            }
                        },
                        function (unique) {
                            scope.notUnique = true;
                            scope.displayMessage = false;
                            scope.checkingUniqueness = false;
                            scope.groupName = "";
                            scope.resultMessage = "An error has happened. Ply try again";
                        }
                    );
                }
            }
        };
    });

angular.module('socialViz')
    .directive('ngAddInteraction', function ($http, $compile, $location, $q) {
        return {
            restrict: 'E',
            replace: false,
            template:
            "<div><span>" +
                "Loading..." +
                "<img src='/images/spinner.gif' style='height: 100px; width: 100px;'/>" +
            "</span></div>",
            scope: {
                token: '@token'
            },
            controller: ['$scope', function ngChoosePersonController($scope) {
                $scope.notUnique = true;
                $scope.displayMessage = false;
                $scope.checkingUniqueness = false;
                $scope.interactionName = "";

                $scope.sending = false;
                $scope.resultMessage = null;
            }],
            link: function link(scope, element, attrs) {
                var pageTemplate =
                    "<div>" +
                        "<h4>Add keyword</h4>" +
                        "<div ng-if='resultMessage'><span ng-bind='resultMessage'></span></div>" +
                        "<form ng-submit='addInteraction()' class='form-horizontal' >" +
                            "<div class='form-group'>" +
                                "<label for='name'>Name:</label><br>" +
                                "<input ng-unique-element name='name' id='name' class='name'" +
                                        "token='{{token}}' " +
                                        "not-unique='notUnique' " +
                                        "display-message='displayMessage' " +
                                        "checking-uniqueness='checkingUniqueness' " +
                                        "url='/interactions/rest/unique-interaction'" +
                                        "param-name='name'" +
                                        "data-ng-model='interactionName' " +
                                        "required='required'>" +
                                "<img src='/images/spinner.gif' ng-show='checkingUniqueness' style='height: 50px; width: 50px;'/>" +
                                "<span class='glyphicon glyphicon-ok' class='tick' ng-show='notUnique===false'></span>" +
                                "<span ng-show='displayMessage'><br>Keyword already exists<br></span>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<label for='groupSelect'>Select group:</label><br>" +
                                "<select name='groupSelect' id='groupSelect'>" +
                                "</select>" +
                            "</div>" +
                            "<div class='form-group' >" +
                                "<label for='parentSelect'>Select parent(or leave blank):</label><br>" +
                                "<select name='parentSelect' id='parentSelect'>" +
                                "</select>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<div class='col-sm-12 col-md-12 col-lg-12'>" +
                                    "<label for='peopleSelect'>" +
                                        "Select connected people:" +
                                        "<ng-clearable-search filter='membersFilter'></ng-clearable-search>" +
                                    "</label><br>" +
                                    "<ul class='peopleList'>" +
                                        "<li ng-repeat='person in people | filter : membersFilter'>" +
                                            "<label class='align-middle'>" +
                                                "<input ng-model='chosenPeople[person.index]' ng-true-value='{{person.id}}' ng-false-value='null' type='checkbox'/>" +
                                                "<span ng-bind='person.name'></span>" +
                                            "</label>" +
                                        "</li>" +
                                    "</ul>" +
                                "</div>" +
                            "</div>" +
                            "<button type='submit' class='btn btn-default' ng-disabled='notUnique || selectedGroup==null'>" +
                                "Add" +
                            "</button>" +
                            "<img src='/images/spinner.gif' style='height: 100px; width: 100px;' ng-show='sending'/>" +
                        "</form>" +
                    "</div>";

                var $groupSelect = null;
                var $parentSelect = null;
                
                scope.addInteraction = function () {
                    if(!scope.sending) {
                        console.log("adding interaction");
                        scope.sending = true;

                        console.log(scope.selectedGroup);

                        var fd = new FormData();
                        fd.append("name", scope.interactionName);
                        fd.append("group", scope.selectedGroup.id);
                        fd.append("parent", scope.selectedParent != null ? scope.selectedParent.id : -1);
                        fd.append("people", scope.chosenPeople);

                        $http({
                            method: 'POST',
                            url: $location.protocol() + '://' + location.host + '/interactions/rest/add-interaction',
                            data: fd,
                            headers: {
                                'X-CSRF-TOKEN': scope.token,
                                'Content-Type': undefined
                            }
                        }).then(
                            function success(success) {
                                scope.sending = false;
                                if(success.data) {
                                    scope.resultMessage = "Hierarchy updated successfully";
                                } else {
                                    scope.resultMessage = "An error has occurred while updating hierarchy.<br>Please try again";
                                }
                                scope.loadForm();
                            },
                            function error(error) {
                                scope.sending = false;
                                scope.resultMessage = "An error has occurred while updating hierarchy.<br>Please try again";
                                scope.loadForm();                          }
                        );
                    }
                };

                scope.$watch('$viewContentLoaded', function (){
                    scope.loadForm();
                });

                scope.loadForm = function(){
                    scope.notUnique = true;
                    scope.displayMessage = false;
                    scope.checkingUniqueness = false;
                    scope.interactionName = "";
                    $groupSelect = null;
                    $parentSelect = null;

                    var contentPromises = [];

                    contentPromises.push(
                        $http.get('/rest/people.json')
                            .then(
                                function success(success) {
                                    scope.people = success.data;
                                    for(var i=0; i<scope.people.length; ++i) {
                                       scope.people[i].index = i;
                                    }
                                },
                                function error(error) {
                                    console.error("FAILED TO load unassigned people: " + error);
                                }
                            )
                    );

                    contentPromises.push(
                        $http.get('/rest/interactions.json')
                            .then(
                                function success(success) {
                                    scope.hierarchicalInteractions = success.data;
                                },
                                function error(error) {
                                    console.error("FAILED TO load unassigned people: " + error);
                                }
                            )
                    );


                    $q.all(contentPromises)
                        .then(
                            function success(success) {
                                scope.interactionByGroup = {};
                                scope.chosenPeople = new Array(scope.people.length).fill(null);

                                if(scope.hierarchicalInteractions == null || scope.hierarchicalInteractions.length==0) {
                                    return;
                                }

                                for (var i = 0; i < scope.hierarchicalInteractions.length; ++i) {
                                    scope.interactionByGroup[scope.hierarchicalInteractions[i].id] = [];

                                    scope.parseHierarchicalInteractions(scope.hierarchicalInteractions[i].types,
                                        scope.interactionByGroup[scope.hierarchicalInteractions[i].id]);
                                }

                                scope.selectedGroup = scope.hierarchicalInteractions[0];
                                scope.selectedGroupList = scope.interactionByGroup[scope.selectedGroup.id];
                                scope.selectedParent = null;

                                var form = angular.element(pageTemplate);
                                element.children().first().replaceWith(form);
                                $compile(element.contents())(scope);

                                $groupSelect = $('#groupSelect').selectize({
                                    maxItems: 1,
                                    valueField: 'id',
                                    labelField: 'name',
                                    searchField: 'name',
                                    sortField: {field: 'name'},
                                    options: scope.hierarchicalInteractions,
                                    items: [scope.selectedGroup.id],
                                    create: false,
                                    allowEmptyOption: false,
                                    onChange: scope.groupSelectEventhandler()
                                });

                                $parentSelect = $('#parentSelect').selectize({
                                    maxItems: 1,
                                    valueField: 'id',
                                    labelField: 'name',
                                    searchField: 'name',
                                    sortField: {field: 'name'},
                                    options: scope.selectedGroupList,
                                    items: [],
                                    create: false,
                                    allowEmptyOption: true,
                                    onChange: scope.parentSelectEventhandler()
                                });

                            },
                            function error(error) {
                                console.error("FAILED TO load social interactions: " + error);
                            }
                        );
                };

                scope.parseHierarchicalInteractions = function(interactions, groupList) {
                    for (var i = 0; i < interactions.length; ++i) {
                        groupList.push(interactions[i]);

                        if(interactions[i].subInteractions !== null) {
                            scope.parseHierarchicalInteractions(interactions[i].subInteractions, groupList);
                        }
                    }
                };

                scope.groupSelectEventhandler = function () {
                    return function () {
                        var id = arguments[0];
                        scope.selectedGroup = null;
                        scope.selectedParent = null;

                        scope.selectedGroupList = id ? scope.interactionByGroup[id] : [];
                        for(var i=0; i < scope.hierarchicalInteractions.length; ++i) {
                            if(scope.hierarchicalInteractions[i].id === id) {
                                scope.selectedGroup = scope.hierarchicalInteractions[i];
                                break;
                            }
                        }
                        if(scope.selectedGroup == null) {
                            console.warn("Selected group is null");
                            console.warn(scope.selectedGroupList);
                        }

                        // fetch the instance
                        var selectize = $parentSelect[0].selectize;
                        //update potential parent list to choose from
                        selectize.clearOptions();
                        selectize.addOption(scope.selectedGroupList);
                        selectize.clear();

                        scope.$apply();
                    };
                };

                scope.parentSelectEventhandler = function () {
                    return function () {
                        var id = arguments[0];
                        scope.selectedParent = null;
                        for(var i=0; i<scope.selectedGroupList.length; ++i) {
                            if(scope.selectedGroupList[i].id === id) {
                                scope.selectedParent = scope.selectedGroupList[i];
                                break;
                            }
                        }
                    }
                };
            }
        };
    });
