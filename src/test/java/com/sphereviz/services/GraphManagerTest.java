package com.sphereviz.services;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsGroupsRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.model.graph.SocialInteractionGroupImpl;
import com.sphereviz.model.graph.SocialInteractionImpl;
import com.sphereviz.model.json_model.JsonInteraction;
import com.sphereviz.model.json_model.JsonInteractionGroup;
import com.sphereviz.model.json_model.JsonPerson;
import com.sphereviz.services.graph.GraphManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Tests for {@link com.sphereviz.services.graph.GraphManagerImpl}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GraphManagerTest {
    @Autowired
    private GraphManager graphManager;

    @Autowired
    private NewPeopleRepository peopleRepository;
    @Autowired
    private SocialInteractionsGroupsRepository groupsRepository;
    @Autowired
    private SocialInteractionsRepository socialInteractionsRepository;


    @Test
    public void getInteractions() {
        List<JsonInteractionGroup> interactions = graphManager.getInteractionsGroups();
        List<SocialInteractionGroupImpl> databaseInteractions = groupsRepository.findAll();

        assertThat(interactions.size(), is(databaseInteractions.size()));
        int totalCount = 0;
        for(JsonInteractionGroup jsongroup : interactions){
            totalCount += countInteractions(jsongroup.getTypes());
        }
        assertThat(totalCount, equalTo(socialInteractionsRepository.findAll().size()));
    }

    private int countInteractions(List<JsonInteraction> interactions) {
        int count = 0;
        for(JsonInteraction interaction : interactions) {
            ++count;
            if(interaction.hasSubInteractions()){
                count += countInteractions(interaction.getSubInteractions());
            }
        }
        return count;
    }

    @Test
    public void getPeople() {
        List<JsonPerson> jsonPeople = graphManager.getPeople();
        assertThat(jsonPeople.size(), is(peopleRepository.findAll().size()));

        for(JsonPerson person : jsonPeople) {
            assertThat(peopleRepository.findById(Long.parseLong(person.getID())), is(notNullValue()));
        }
    }

    @Test
    public void updateSocialInteractionsHierarchy() {
        SocialInteractionGroupImpl group = new SocialInteractionGroupImpl("TestGroup");

        SocialInteractionImpl parent = new SocialInteractionImpl("test parent", group);
        group.addSocialInteractionToGroup(parent);

        SocialInteractionImpl child = new SocialInteractionImpl("test child", parent);
        parent.addSubInteraction(child);

        SocialInteractionImpl childChild = new SocialInteractionImpl("test child child", child);
        child.addSubInteraction(childChild);

        group = groupsRepository.saveAndFlush(group);
        parent = socialInteractionsRepository.saveAndFlush(parent);
        child = socialInteractionsRepository.saveAndFlush(child);
        childChild = socialInteractionsRepository.saveAndFlush(childChild);

        assertThat(
                graphManager.updateSocialInteractionsHierarchy(parent.getId(), child.getId(), group.getId()),
                is(false)
        );

        assertThat(
                graphManager.updateSocialInteractionsHierarchy(childChild.getId(), parent.getId(), group.getId()),
                is(true)
        );

        socialInteractionsRepository.delete(childChild.getId());
        socialInteractionsRepository.delete(child.getId());
        socialInteractionsRepository.delete(parent.getId());
        groupsRepository.delete(group.getId());
    }
}
