package com.sphereviz.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Map images directory
 */
@Configuration
public class CustomWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {
    private static final Logger log = Logger.getLogger(CustomWebMvcConfigurerAdapter.class);

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        final Path currentRelativePath = Paths.get("photos");
        final String absolutePath = currentRelativePath.toAbsolutePath().toString().replaceAll("\\\\", "/");
        final String photosLocation = "file:///" + absolutePath + "/";

        registry.addResourceHandler("/photos/**").addResourceLocations(photosLocation);
        super.addResourceHandlers(registry);
        log.info("configured images mapping");
    }
}
