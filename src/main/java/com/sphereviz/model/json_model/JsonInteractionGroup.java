package com.sphereviz.model.json_model;

import com.sphereviz.model.graph.SocialInteractionGroup;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Json representation of {@link com.sphereviz.model.graph.SocialInteractionGroup}
 */
public class JsonInteractionGroup {
    private String id;
    private String name;
    private List<JsonInteraction> types;


    public JsonInteractionGroup(){}

    public JsonInteractionGroup(SocialInteractionGroup group){
        this.id = String.valueOf(group.getId());
        this.name = group.getName();
        this.types = group.getSocialInteractions().stream()
                                                    .map(JsonInteraction::new)
                                                    .collect(Collectors.toList());
    }

    public JsonInteractionGroup(String id, String name, List<JsonInteraction> types) {
        this.id = id;
        this.name = name;
        this.types = types;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JsonInteraction> getTypes() {
        return types;
    }

    public void setTypes(List<JsonInteraction> types) {
        this.types = types;
    }
}
