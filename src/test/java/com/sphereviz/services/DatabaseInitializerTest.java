package com.sphereviz.services;

import com.sphereviz.data.graph.SocialInteractionsRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lesscss.deps.org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Tests for database initialization form json file
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DatabaseInitializerTest {
    private static final String JSON_FILE = "data.json";

    private static final String GROUP_INTERACTIONS = "interactions";
    private static final String SOCIAL_INTERACTIONS = "social-interactions";
    private static final String SUB_INTERACTIONS = "sub-interactions";

    @Autowired
    private SocialInteractionsRepository socialInteractionsRepository;

    private int interactionsCount;


    @Test
    public void testSocialInteractionCount() {
        //count all social interactions in json file
        interactionsCount = 0;
        try(
                final FileInputStream fisTargetFile = new FileInputStream(new File(JSON_FILE))
        ) {
            final JSONObject rootObject = new JSONObject( IOUtils.toString(fisTargetFile, "UTF-8") );
            System.out.println("Successfully loaded JSON Object from File...");

            final JSONArray groups = rootObject.getJSONArray(SOCIAL_INTERACTIONS);
            for (int i=0; i< groups.length(); ++i) {
                final JSONArray interactions = groups.getJSONObject(i).getJSONArray(GROUP_INTERACTIONS);
                countSubInteractions(interactions);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Couldn't find JSON file", e);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't process JSON file", e);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assertThat(socialInteractionsRepository.findAll().size(), is(interactionsCount));
    }

    private void countSubInteractions(final JSONArray subInteractions) throws JSONException {
        for (int i=0; i< subInteractions.length(); ++i) {
            final JSONObject interaction = subInteractions.getJSONObject(i);
            ++interactionsCount;

            if(interaction.has(SUB_INTERACTIONS)){
                countSubInteractions(interaction.getJSONArray(SUB_INTERACTIONS));
            }
        }
    }

}
