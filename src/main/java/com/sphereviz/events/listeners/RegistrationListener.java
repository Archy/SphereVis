package com.sphereviz.events.listeners;

import com.sphereviz.events.types.OnRegistrationCompleteEvent;
import com.sphereviz.model.users.ConfirmationToken;
import com.sphereviz.services.mail.MailService;
import com.sphereviz.services.mail.messages.MessageCreator;
import com.sphereviz.services.users.UserService;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

/**
 * Registration event listener.
 * Sends email with confirmation link
 */
@Component
public class RegistrationListener {
    private final UserService userService;
    private final MailService mailService;


    @Inject
    public RegistrationListener(@NotNull final UserService userService,
                                @NotNull final MailService mailService) {
        this.userService = userService;
        this.mailService = mailService;
    }

    //@Async
    @EventListener
    public void onRegistrationEvent(@NotNull final OnRegistrationCompleteEvent event) {
        final ConfirmationToken token = userService.createConfirmationToken(event.getUser());
        //send email to user
        final String confirmationUrl = event.getUrl() + "/registration/confirm?token=" + token.getToken();
        mailService.sendMail(event.getUser().getEmail(), "Email confirmation",
                MessageCreator.createConfirmationMessage(confirmationUrl, event.getUser().getEmail()));
    }
}