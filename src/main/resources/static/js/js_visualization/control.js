angular.module('socialViz').controller('visController', function ($scope, $http, $location, syncSRV) {
    $scope.members = [];
    $scope.allInteractions = [];
    $scope.hierarchicalInteractions = [];
    $scope.graph = {};
    $scope.navMaxHeight = { height: 0 };

    $scope.allPromises = null;
    $scope.nextActive = null;

    $scope.filterStates = {
        activeMembersFilterActive: false,
        activeInteractionsFilterActive: false
    };

    $scope.programState = programStateEnum.LOADING;

    $scope.$watch('$viewContentLoaded', function () {
        $scope.allPromises = [];
        $scope.allPromises.push($http.get('/rest/people.json')
            .then(
                function success(success) {
                    console.log("loaded people data");
                    $scope.members = success.data;
                    //create and set active to false
                    for (var i = 0; i < $scope.members.length; ++i) {
                        $scope.members[i].active = false;
                    }
                },
                function error(error) {
                    console.error("FAILED TO load people data: " + error);
                }
            )
        );

        $scope.allPromises.push($http.get('/rest/interactions.json')
            .then(
                function success(success) {
                    console.log("loaded interactions json");
                    $scope.hierarchicalInteractions = success.data;
                    //create and set active to false
                    for (var i = 0; i < $scope.hierarchicalInteractions.length; ++i) {
                        $scope.hierarchicalInteractions[i].active = false;
                        $scope.hierarchicalInteractions[i].filter = '';

                        parseHierarchicalInteractions($scope.hierarchicalInteractions[i].types, $scope.hierarchicalInteractions[i].name);
                    }
                    //set active type of social interactions
                    s = $scope.hierarchicalInteractions[0];

                    s.active = !s.active;
                    $scope.currActive = s;

                    $scope.socialInteraction = s.types;
                    $scope.interactionsFilter = s.filter;
                },
                function error(error) {
                    console.error("FAILED TO load hierarchical social interactions list: " + error);
                }
            )
        );

        $scope.allPromises.push($http.get('/rest/graph.json')
            .then(
                function success(success) {
                    console.log("loaded graphs json");
                    $scope.graph = success.data;
                },
                function error(error) {
                    console.error("FAILED TO load social interactions data: " + error);
                }
            )
        );
    });

    function parseHierarchicalInteractions(interactions, typeName) {
        for (var i = 0; i < interactions.length; ++i) {
            interactions[i].active = false;
            interactions[i].typeName = typeName;

            $scope.allInteractions.push(interactions[i]);
            if(interactions[i].subInteractions !== null) {
                parseHierarchicalInteractions(interactions[i].subInteractions, typeName);
            }
        }
    }

    $scope.swapSpheres = function () {
        if ($scope.programState === programStateEnum.READY) {
            $scope.programState = programStateEnum.CHANGING;
            syncSRV.beginSpheresSwitch();
        }
    };
});

angular.module('socialViz').directive("ngLeftNav", function (syncSRV) {
    return {
        restrict: "EA",
        controller: ["$scope", function ($scope) {
            $scope.dataTableHeightLeft = '500px';
            $scope.membersFilter = "";

            $scope.activeMembersFilter = function (member) {
                if($scope.filterStates.activeMembersFilterActive === false)
                    return true;
                else
                    return member.active;
            };
        }],
        link: function postLink(scope, element, attrs) {
            scope.$watch('navMaxHeight', function (newValue, oldValue) {
                var totalHeight = 0;
                for (var i = 0; i < 3; i++) {
                    var currDiv = angular.element(element)[0].children[i];
                    totalHeight += $(currDiv).outerHeight(true);
                }

                var dataHeight = scope.navMaxHeight.height - totalHeight;
                var dataTable = element[0].getElementsByClassName('dataTable');

                scope.dataTableHeightLeft = dataHeight + 'px';
            }, true);

            scope.setActiveLeft = function (s) {
                if (scope.programState === programStateEnum.READY) {
                    scope.filterStates.activeMembersFilterActive = false;
                    scope.filterStates.activeInteractionsFilterActive = true;

                    syncSRV.sync(s);
                }
            };

            scope.$on('updatedScroll', function () {
                var $dataList = (element[0].querySelector('.dataTable'));
                var liObjects = $dataList.getElementsByTagName("li");

                for (var i = 0; i < liObjects.length; i++) {
                    var li = liObjects[i];
                    if (li.innerText === syncSRV.scrollTo.name.toUpperCase()) {
                        $dataList.scrollTop = $dataList.scrollTop + $(li).position().top;
                        scope.filterStates.activeMembersFilterActive = false;
                        scope.filterStates.activeInteractionsFilterActive = true;
                        break;
                    }
                }
            });
        }
    }
});

angular.module('socialViz').directive("ngRightNav", function (syncSRV) {
    return {
        restrict: "EA",
        controller: ["$scope", function ($scope) {
            $scope.socialInteraction = [];
            $scope.currActive = {};
            $scope.dataTableHeightRight = '500px';

            $scope.interactionsFilter = "";

            $scope.click = function (s) {
                if ($scope.programState === programStateEnum.READY) {
                    if ($scope.currActive !== null) {
                        $scope.currActive.filter = $scope.interactionsFilter;
                    }

                    s.active = !s.active;
                    $scope.currActive = s;

                    $scope.socialInteraction = s.types;
                    $scope.interactionsFilter = s.filter;
                }
            };
        }],
        link: function postLink(scope, element, attrs) {
            scope.$watch('[navMaxHeight, currActive]', function (newValue, oldValue) {
                var totalHeight = 0;
                for (var i = 0; i < 3; i++) {
                    var currDiv = angular.element(element)[0].children[i];
                    totalHeight += $(currDiv).outerHeight(true);
                }

                var dataHeight = scope.navMaxHeight.height - totalHeight;
                var dataTable = element[0].getElementsByClassName('dataTable');
                scope.dataTableHeightRight = dataHeight + 'px';
            }, true);

            scope.$on('updatedScroll', function () {

                var $dataList = (element[0].querySelector('.dataTable'));
                var liObjects = $dataList.getElementsByTagName("li");

                for (var i = 0; i < liObjects.length; i++) {
                    var li = liObjects[i];
                    if (li.innerText === syncSRV.scrollTo.name.toUpperCase()) {
                        $dataList.scrollTop = $dataList.scrollTop + $(li).position().top;
                        scope.filterStates.activeMembersFilterActive = true;
                        scope.filterStates.activeInteractionsFilterActive = false;
                        break;
                    }
                }
            });

            scope.$on("updatedSocialInteraction", function () {
                var s = null;
                for (var i = 0; i < scope.hierarchicalInteractions.length; ++i) {
                    if (scope.hierarchicalInteractions[i].name === syncSRV.interactionType) {
                        s = scope.hierarchicalInteractions[i];
                        break;
                    }
                }
                console.log(syncSRV.interactionType);


                if (scope.currActive !== null) {
                    scope.currActive.filter = scope.interactionsFilter;
                }

                s.active = !s.active;
                scope.currActive = s;

                scope.socialInteraction = s.types;
                scope.interactionsFilter = s.filter;

                scope.$apply();
            });
        }
    };
});

angular.module('socialViz').directive("ngHierarchicalList", function ($compile, syncSRV) {
    return {
        restrict: "E",
        template:
            "<ul class='list-unstyled'>" +
                "<li ng-repeat=\"interaction in ngInteractions | orderBy : 'name'\" class='parentInteraction'>" +
                    "<ul class='list-unstyled'>" +
                        "<li class='interaction' ng-bind='interaction.name' ng-class='{active: interaction.active}' ng-click='setActiveRight(interaction)' ng-hide='hideInteraction(interaction)'>" +
                        "</li>" +
                        "<li ng-if='interaction.subInteractions' class='sublist'>" +
                            "<ng-hierarchical-list ng-interactions='interaction.subInteractions' " +
                                "program-state='programState' " +
                                "filter-states='filterStates' " +
                                "interactions-filter='interactionsFilter' " +
                            ">" +
                        "</li>" +
                    "</ul>" +
                "</li>" +
            "</ul>"
        ,
        controller: ["$scope", function ($scope) {
        }],
        scope: {
            ngInteractions: '=',
            programState: '=',
            filterStates: '=',
            interactionsFilter: "="
        },
        link: function postLink(scope, element, attrs) {
            scope.setActiveRight = function (s) {
                if (scope.programState === programStateEnum.READY) {
                    scope.filterStates.activeMembersFilterActive = true;
                    scope.filterStates.activeInteractionsFilterActive = false;
                    syncSRV.sync(s);
                }
            };

            scope.hideInteraction = function(interaction) {
                var result = false;

                if(scope.filterStates.activeInteractionsFilterActive === true) {
                    //if show only active interactions
                    result = !interaction.active;
                }
                if(result === false) {
                    //interaction should still be shown check interaction name filter
                    if(scope.interactionsFilter != null && scope.interactionsFilter !== "") {
                        result = interaction.name.indexOf(scope.interactionsFilter) === -1;
                    }
                }
                return result;
            };
        }
    };
});
