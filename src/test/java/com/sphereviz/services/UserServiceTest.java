package com.sphereviz.services;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.users.ConfirmationTokenRepository;
import com.sphereviz.data.users.UserRepository;
import com.sphereviz.model.graph.Person;
import com.sphereviz.model.users.ConfirmationToken;
import com.sphereviz.model.users.User;
import com.sphereviz.services.users.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Tests for {@link com.sphereviz.services.users.UserServiceImpl}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    private static final String EMAIL = "jan.majkutewicz@gmail.com";
    private static final String PASSWORD = "password";

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConfirmationTokenRepository tokenRepository;
    @Autowired
    private NewPeopleRepository peopleRepository;

    @Test
    public void addUserAndAssignPerson() {
        //only admin account id database
        assertThat(userRepository.findAll().size(), is(1));
        assertThat(userService.isEmailUnique(EMAIL), is(true));

        Optional<User> userOptional = userService.addUser(EMAIL, PASSWORD);
        assertThat(userOptional.isPresent(), is(true));

        User user = userOptional.get();
        //create token
        ConfirmationToken token = userService.createConfirmationToken(user);
        assertThat(token, is(notNullValue()));
        boolean confirmationResult = userService.confirmUserEmail(token.getToken());
        assertThat(confirmationResult, is(true));

        //get unassigned person
        List<Person> unassigned = peopleRepository.findAllUnassigned();
        assertThat(unassigned.isEmpty(), is(false));

        Person person = unassigned.get(0);
        boolean assignmentResult =  userService.assignPerson(EMAIL, person.getId());
        assertThat(assignmentResult, is(true));

        Person assignedPerson = userService.getUserPerson(EMAIL);
        assertThat(assignedPerson.getId(), is(person.getId()));

        User assignedUser = userService.getPersonUser(person.getId());
        assertThat(assignedUser.getEmail(), equalTo(EMAIL));
    }
}
