package com.sphereviz.controllers.registration;

import com.sphereviz.model.helpers.Message;
import com.sphereviz.services.users.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

/**
 * Controller for user email confirmation. Handles url with tokens and confirmation result
 */
@Controller
public class ConfirmationController {
    private final UserService userService;


    @Inject
    public ConfirmationController(@NotNull final UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/registration/confirm")
    public String confirmEmail(@RequestParam("token") final String token){
        if(userService.confirmUserEmail(token)){
            return "redirect:/registration/confirmation_success";
        } else {
            return "redirect:/registration/confirmation_failure";
        }
    }

    @RequestMapping("/registration/confirmation_success")
    public String success(final Model model){
        model.addAttribute("message",
                new Message("Email confirmation was successful", "/", "Back to main page"));
        return "template";
    }

    @RequestMapping("/registration/confirmation_failure")
    public String failure(final Model model){
        model.addAttribute("message",
                new Message("Email confirmation failed", "/registration", "Back to main page"));
        return "template";
    }

}
