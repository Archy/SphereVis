package com.sphereviz.utils;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Tests for {@link StringSplitter}
 */
public class StringSplitterTest {

    @Test
    public void testSplitting() {
        ImmutableMultiLineString result = StringSplitter.splitString("aaaaaa-bb-cc-dddd", 5);
        assertThat(result.getLinesCount(), is(3));
        assertThat(result.getMaxLineWidth(), is(6));
    }
}
