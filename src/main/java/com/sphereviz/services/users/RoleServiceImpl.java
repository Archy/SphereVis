package com.sphereviz.services.users;

import com.sphereviz.data.users.RolesRepository;
import com.sphereviz.model.users.Role;
import com.sphereviz.model.users.RoleEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


@Service
public class RoleServiceImpl implements RoleService {
    private final RolesRepository rolesRepository;


    @Inject
    public RoleServiceImpl(@NotNull final RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    @Override
    @NotNull
    public List<Role> findAllByName(RoleEnum ... roles) {
        if(roles == null || roles.length == 0){
            throw new IllegalArgumentException("At least 1 role is required");
        }

        final List<Role> list = new ArrayList<>();
        for(RoleEnum role : roles){
            final Role entity = rolesRepository.findByName(role.getName());
            if(entity != null) {
                list.add(entity);
            }
        }
        return list;
    }

    @Nullable
    @Override
    public Role findByName(@NotNull final RoleEnum role) {
        return rolesRepository.findByName(role.getName());
    }
}
