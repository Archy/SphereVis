package com.sphereviz.security.filters;

import com.sphereviz.model.users.RoleEnum;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * Filter that redirects user with unconfirmed email
 */
@Component
public class UnconfirmedUserRedirectFilter extends OncePerRequestFilter {
    private static final Logger log = Logger.getLogger(UnconfirmedUserRedirectFilter.class);

    @Override
    protected void doFilterInternal(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse,
                                    final FilterChain filterChain) throws ServletException, IOException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String currentUrl = UrlUtils.buildRequestUrl(httpServletRequest);

        if(authentication != null && !"/login/unconfirmed".equals(currentUrl)){
            Collection<SimpleGrantedAuthority> authorities =
                    (Collection<SimpleGrantedAuthority>) authentication.getAuthorities();
            if(authorities.contains(new SimpleGrantedAuthority(RoleEnum.UNCONFIRMED.getName()))){
                httpServletResponse.sendRedirect("/login/unconfirmed");
                log.info("Redirected unconfirmed user: " + authentication.getName());
                return;
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
