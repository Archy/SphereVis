package com.sphereviz.services.mail;

/**
 * Service for sending confirmation links to new registered users
 */
public interface MailService {
    /**
     * Send email via gmail server
     * @param to new user email
     * @param subject subject
     * @param msg actual message
     */
    void sendMail(String to, String subject, String msg);
}
