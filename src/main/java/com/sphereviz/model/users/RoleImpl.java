package com.sphereviz.model.users;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(
        name = "role"
)
public class RoleImpl implements Role {
    @Id
    @GeneratedValue(strategy=IDENTITY)
    private long id;
    @Column(nullable = false)
    private String name;


    public RoleImpl() {
    }

    public RoleImpl(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleImpl role = (RoleImpl) o;

        if (id != role.id) return false;
        return name.equals(role.name);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        return result;
    }
}
