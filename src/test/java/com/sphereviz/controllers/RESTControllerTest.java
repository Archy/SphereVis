package com.sphereviz.controllers;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsGroupsRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.helpers.TestUtil;
import com.sphereviz.model.graph.SocialInteractionImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RESTControllerTest {
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private NewPeopleRepository peopleRepository;
    @Autowired
    private SocialInteractionsRepository socialInteractionsRepository;
    @Autowired
    private SocialInteractionsGroupsRepository groupsRepository;

    private List<String> parsedInteractions = new ArrayList<>();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    /**
     * Test for checking if json list of social interactions contains all of them
     */
    @Test
    public void testGettingInteractionList() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/interactions.json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(groupsRepository.findAll().size())))
                .andReturn();

        JSONArray groups = new JSONArray(result.getResponse().getContentAsString());
        for(int i=0; i<groups.length(); ++i) {
            parseInteractions(groups.getJSONObject(i).getJSONArray("types"));
        }

        assertThat(parsedInteractions, containsInAnyOrder(
                socialInteractionsRepository.findAll()
                                            .stream()
                                            .map(SocialInteractionImpl::getName)
                                            .collect(Collectors.toList())
                                            .toArray()
        ));
    }

    private void parseInteractions(JSONArray ineractions) throws JSONException {
        for(int i=0; i<ineractions.length(); ++i) {
            JSONObject interaction = ineractions.getJSONObject(i);
            parsedInteractions.add(interaction.getString("name"));
            if(interaction.has("subInteractions") && !interaction.isNull("subInteractions")){
                parseInteractions(interaction.getJSONArray("subInteractions"));
            }
        }
    }

    /**
     * Test for checking if json map is covered by interactions and people lists
     */
    @Test
    public void testGettingGraph() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/graph.json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(peopleRepository.findAll().size())))
                .andReturn();
        JSONObject map = new JSONObject(result.getResponse().getContentAsString());

        result = mockMvc.perform(get("/rest/people.json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(peopleRepository.findAll().size())))
                .andReturn();
        JSONArray people = new JSONArray(result.getResponse().getContentAsString());

        result = mockMvc.perform(get("/rest/all-interactions.json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(socialInteractionsRepository.findAll().size())))
                .andReturn();
        JSONArray interactions = new JSONArray(result.getResponse().getContentAsString());

        for(int i=0; i<people.length(); ++i) {
            for(int j=0; j<interactions.length(); ++j) {
                final String personId = people.getJSONObject(i).getString("id");
                final String interactionId = interactions.getJSONObject(j).getString("id");

                assertThat(map.has(personId), is(true));
                assertThat(map.getJSONObject(personId).has(interactionId), is(true));
            }
        }
    }
}
