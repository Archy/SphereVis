package com.sphereviz.controllers.registration;

import com.sphereviz.services.users.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * Controller for async email uniqueness checking
 */
@RestController
public class RegistrationREST {
    private final UserService userService;


    @Inject
    public RegistrationREST(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/rest/email")
    public boolean isMailUnique(@RequestParam final String email){
        return userService.isEmailUnique(email);
    }
}
