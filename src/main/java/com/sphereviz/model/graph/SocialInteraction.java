package com.sphereviz.model.graph;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Entity representing social interaction in social network
 */
public interface SocialInteraction extends VisEntity {
    /**
     *
     * @return id
     */
    long getId();

    /**
     *
     * @return social interaction name
     */
    String getName();

    /**
     *
     * @return {@link SocialInteractionGroup} that aggregates interaction
     */
    SocialInteractionGroup getGroup();

    /**
     *
     * @return true if this interaction has sub-interactions
     */
    boolean hasSubInteractions();

    /**
     *
     * @return set of all child {@link SocialInteraction} of current interaction
     */
    @NotNull
    Set<SocialInteraction> getSubInteractions();
}
