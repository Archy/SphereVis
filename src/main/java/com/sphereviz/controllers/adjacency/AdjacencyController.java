package com.sphereviz.controllers.adjacency;

import com.sphereviz.model.graph.Person;
import com.sphereviz.model.json_model.JsonPerson;
import com.sphereviz.model.users.RoleEnum;
import com.sphereviz.services.graph.GraphManager;
import com.sphereviz.services.users.UserService;
import org.apache.log4j.Logger;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

/**
 * Main adjacency configuration controller, sends proper html to user(admin, user without/ with assigned person)
 */
@Controller
public class AdjacencyController {
    private static final Logger log = Logger.getLogger(AdjacencyController.class);

    private final UserService userService;
    private final GraphManager graphManager;


    @Inject
    public AdjacencyController(@NotNull final UserService userService,
                               @NotNull final GraphManager graphManager) {
        this.userService = userService;
        this.graphManager = graphManager;
    }

    @GetMapping("/adjacency")
    public String getAdjacency(@NotNull final Model model){
        final UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(userDetails.getAuthorities().contains(new SimpleGrantedAuthority(RoleEnum.ADMIN.getName()))){
            //admin - edit all
            model.addAttribute("members", graphManager.getPeople());
            model.addAttribute("interactions", graphManager.getInteractions());
            model.addAttribute("adjacency", graphManager.getGraph());

            return "adjacency/admin";
        } else {
            final Person person = userService.getUserPerson(userDetails.getUsername());
            if(person == null){
                //choose person
                return "adjacency/choose";
            } else {
                //edit own adjacency
                model.addAttribute("person", new JsonPerson(person));
                return "adjacency/edit";
            }
        }
    }

    @PostMapping("/adjacency")
    public String postAdjacency() {
        return "adjacency/edit";
    }
}
