package com.sphereviz.controllers.registration;

import com.sphereviz.events.types.OnRegistrationCompleteEvent;
import com.sphereviz.model.helpers.Message;
import com.sphereviz.model.users.User;
import com.sphereviz.services.users.UserService;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Main registration controller. Handles form sending and receiving, as well as registration outcome
 */
@Controller
public class RegistrationController {
    private static final Logger log = Logger.getLogger(RegistrationController.class);

    private final ApplicationEventPublisher eventPublisher;
    private final UserService userService;


    @Inject
    public RegistrationController(@NotNull final ApplicationEventPublisher eventPublisher,
                                  @NotNull final UserService userService) {
        this.eventPublisher = eventPublisher;
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String get() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            //user is logged in
            log.warn("logged user tried to register");
            return "redirect:/";
        } else  {
            return "registration";
        }
    }

    @PostMapping("/registration")
    public String post(@RequestParam(name = "email") final String email,
                       @RequestParam(name = "password") final String password,
                       HttpServletRequest request) {
        final Optional<User> addedUser = userService.addUser(email, password);
        if(addedUser.isPresent()){
            final StringBuffer url = request.getRequestURL();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent
                    (url.substring(0, url.length()-"/registration".length()), addedUser.get()));
            log.info("registration event for '" + addedUser.get().getEmail() + "' published");

            return "redirect:/registration/registration_success";
        } else {
            return "redirect:/registration/registration_failure";
        }
    }

    @RequestMapping("/registration/registration_success")
    public String success(final Model model){
        model.addAttribute("message",
                new Message("Registered successfully", "/", "Back to main page"));
        return "template";
    }

    @RequestMapping("/registration/registration_failure")
    public String failure(final Model model){
        model.addAttribute("message",
                new Message("Registration failure", "/registration", "Back to registration page"));
        return "template";
    }
}
