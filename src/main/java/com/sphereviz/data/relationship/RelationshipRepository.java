package com.sphereviz.data.relationship;

import com.sphereviz.model.graph.PersonImpl;
import com.sphereviz.model.graph.SocialInteractionImpl;
import com.sphereviz.model.relationship.RelationshipImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * {@link RelationshipImpl} repository
 */
@Repository
public interface RelationshipRepository extends JpaRepository<RelationshipImpl, Long> {
    /**
     *
     * @param person {@link com.sphereviz.model.graph.Person}
     * @param socialInteraction {@link com.sphereviz.model.graph.SocialInteraction}
     * @return true if relationship between person and social interaction exists
     */
    @Query("SELECT COUNT(relationship)>0 FROM RelationshipImpl relationship WHERE relationship.person = :person AND relationship.socialInteraction = :socialInteraction")
    boolean relationExists(@Param("person") PersonImpl person, @Param("socialInteraction") SocialInteractionImpl socialInteraction);

    /**
     *
     * @param personId {@link com.sphereviz.model.graph.Person} id
     * @param interactionId {@link com.sphereviz.model.graph.SocialInteraction} id
     * @return {@link RelationshipImpl} if relationship between person and social interaction exists, null otherwise
     */
    @Query("SELECT relationship FROM RelationshipImpl relationship WHERE relationship.person.id = :personId AND relationship.socialInteraction.id = :socialInteractionId")
    RelationshipImpl findByPersonIdAndInteractionId(@Param("personId") long personId, @Param("socialInteractionId") long interactionId);
}
