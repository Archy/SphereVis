package com.sphereviz.services.graph;

import com.sphereviz.model.graph.VisEntity;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Service for {@link VisEntity} management.
 * Used by  {@link com.sphereviz.se_algorithm.SEAlgorithm}
 */
public interface EntityService {
    /**
     * Return list of {@link com.sphereviz.model.graph.SocialInteraction} and {@link com.sphereviz.model.graph.Person}
     * combined
     * @return all {@link VisEntity}
     */
    @NotNull
    List<VisEntity> getAllEntities();

    /**
     * Saves all entities positions on spheres in database
     */
    void safeAllEntitiesPositions();

    /**
     *
     * @param e1 entity of type {@link com.sphereviz.model.graph.SocialInteraction} or {@link com.sphereviz.model.graph.Person}
     * @param e2 entity of type {@link com.sphereviz.model.graph.SocialInteraction} or {@link com.sphereviz.model.graph.Person}
     * @param <T> {@link com.sphereviz.model.graph.SocialInteraction} or {@link com.sphereviz.model.graph.Person} or {@link VisEntity}
     * @param <S> {@link com.sphereviz.model.graph.SocialInteraction} or {@link com.sphereviz.model.graph.Person} or {@link VisEntity}
     * @return true if relationship between entities exists
     */
    <T extends VisEntity, S extends VisEntity> boolean areConnected(@NotNull T e1, @NotNull S e2);
}
