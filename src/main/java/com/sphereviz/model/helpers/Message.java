package com.sphereviz.model.helpers;

/**
 * universal message model used for template.html
 */
public class Message {
    private String title;
    private String href;
    private String message;


    public Message() {
    }

    public Message(String title, String href, String message) {
        this.title = title;
        this.href = href;
        this.message = message;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
