package com.sphereviz.utils;

import java.util.List;

/**
 * Class representing text consisting of multiple lines
 */
public class MultiLineString implements ImmutableMultiLineString {
    private final List<String> lines;
    private final int maxLenght;

    public MultiLineString(List<String> lines, int maxLenght) {
        this.lines = lines;
        this.maxLenght = maxLenght;
    }

    @Override
    public int getLinesCount() {
        return lines.size();
    }

    @Override
    public int getMaxLineWidth() {
        return maxLenght;
    }

    @Override
    public List<String> getLines() {
        return lines;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for(String line : lines) {
            str.append(line).append(" ");
        }
        return str.toString();
    }
}
