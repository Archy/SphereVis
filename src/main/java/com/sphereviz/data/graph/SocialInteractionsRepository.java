package com.sphereviz.data.graph;

import com.sphereviz.model.graph.SocialInteractionImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link SocialInteractionImpl} repository
 */
@Repository
public interface SocialInteractionsRepository extends JpaRepository<SocialInteractionImpl, Long> {
    SocialInteractionImpl findByName(String name);

    SocialInteractionImpl findById(long id);

    List<SocialInteractionImpl> findAllByName(String name);
}
