﻿// -----------------------------------
// Program state
// -----------------------------------
programStateEnum = {
    READY: 0,
    LOADING: 1,
    CHANGING: 2
};

spheresStateEnum = {
    NORMAL: 0,
    SWITCHED: 1
};

// -----------------------------------
// Models handler
// -----------------------------------
function ModelsCreator(ra, rb, recursionLevel) {
    this._recursionLevel = recursionLevel;
    this._ra = ra;
    this._rb = rb;
    var t;
    var outlineWidth = 0.1;
    var lineHeight = 68;
    var labelsCoeff = 0.85;

    var portraits = {};
    var portraitsBorders = {};

    var labels = {};
    var highlightedLabels = {};

    var edges = {};
    var switchedEdges = {};

    var currActive = null;

    var allObjects = [];
    var raycaster = new THREE.Raycaster();

    var spheresState = spheresStateEnum.NORMAL;

    // -----------------------------------
    // Creating portraits and labels
    // -----------------------------------

    this.createPortrait = function(scene, portraitPath, person, visualisationPromises) {
        var loader = new THREE.TextureLoader();
        //create promise
        visualisationPromises.push(new Promise(function (resolve, reject) {
            // load a resource
            loader.load(
                // resource URL
                portraitPath,
                // Function when resource is loaded
                function (texture) {
                    //create material
                    var img = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
                    img.map.needsUpdate = true;
                    img.map.minFilter = THREE.LinearFilter; //to get rid of "The size of your texture is not powers of two" error

                    // create mesh
                    var width = 2;
                    var height = 2;
                    //keep image ascpect ratio
                    if (texture.image.width > texture.image.height) {
                        //keep height = 1, resize width
                        width = texture.image.width / texture.image.height * height;
                    } else {
                        //keep width = 1, resize height
                        height = texture.image.height / texture.image.width * width;
                    }

                    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), img);
                    mesh.overdraw = true;

                    //OUTLINE
                    var outline = new THREE.Geometry();
                    var x = width / 2.0;
                    var y = height / 2.0;

                    //inner
                    outline.vertices.push(new THREE.Vector3(-x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, -y, 0));
                    outline.vertices.push(new THREE.Vector3(-x, -y, 0));
                    x += outlineWidth;
                    y += outlineWidth;
                    //outer
                    outline.vertices.push(new THREE.Vector3(-x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, -y, 0));
                    outline.vertices.push(new THREE.Vector3(-x, -y, 0));
                    //faces
                    //up
                    outline.faces.push(new THREE.Face3(0, 5, 4));
                    outline.faces.push(new THREE.Face3(0, 1, 5));
                    //right
                    outline.faces.push(new THREE.Face3(1, 6, 5));
                    outline.faces.push(new THREE.Face3(1, 2, 6));
                    //down
                    outline.faces.push(new THREE.Face3(7, 6, 2));
                    outline.faces.push(new THREE.Face3(7, 2, 3));
                    //left
                    outline.faces.push(new THREE.Face3(7, 3, 4));
                    outline.faces.push(new THREE.Face3(3, 0, 4));
                    //model creation
                    outline.computeFaceNormals();
                    var outlineMaterial = new THREE.MeshBasicMaterial({ color: 0x00ccff });
                    var outlineMesh = new THREE.Mesh(outline, outlineMaterial);
                    outlineMesh.visible = false;

                    //add to scene and remember
                    scene.add(mesh);
                    scene.add(outlineMesh);

                    portraits[person.name] = mesh;
                    portraitsBorders[person.name] = outlineMesh;

                    allObjects.push(mesh);
                    resolve(mesh);
                },
                // Function called when download progresses
                function (xhr) {
                },
                // Function called when download errors
                function (xhr) {
                    console.error('An error happened when loading portrait: ' + xhr);
                    reject(new Error('Could not load portrait: ' + xhr));
                }
            );
        }));
    };

    this.createLabel = function (scene, labelPath, interaction, interactionType, visualisationPromises) {
        var loader = new THREE.TextureLoader();
        //create promise
        visualisationPromises.push(new Promise(function (resolve, reject) {
            // load a resource
            loader.load(
                // resource URL
                labelPath,
                // Function when resource is loaded
                function (texture) {
                    //create material
                    var img = new THREE.MeshBasicMaterial({ map: texture, transparent: true, alphaTest: 0.5 });
                    img.map.needsUpdate = true;
                    img.map.minFilter = THREE.LinearFilter; //to get rid of "The size of your texture is not powers of two" error

                    // create mesh
                    var lineCount = texture.image.height / lineHeight;
                    var height = lineCount * labelsCoeff;
                    var width =  texture.image.width / lineHeight * labelsCoeff;

                    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), img);
                    mesh.overdraw = true;

                    //add to scene and remember
                    scene.add(mesh);
                    labels[interaction.id] = mesh;
                    allObjects.push(mesh);
                    resolve(mesh);
                },
                // Function called when download progresses
                function (xhr) {
                },
                // Function called when download errors
                function (xhr) {
                    console.error('An error happened when loading portrait: ' + xhr);
                    reject(new Error('Could not load portrait: ' + xhr));
                }
            );
        }));
    };

    this.createHighlightedLabel = function (scene, highlightedLabelPath, interaction, interactionType, visualisationPromises) {
        var loader = new THREE.TextureLoader();
        //create promise
        visualisationPromises.push(new Promise(function (resolve, reject) {
            // load a resource
            loader.load(
                // resource URL
                highlightedLabelPath,
                // Function when resource is loaded
                function (texture) {
                    //create material
                    var img = new THREE.MeshBasicMaterial({ map: texture, transparent: true, alphaTest: 0.5 });
                    img.map.needsUpdate = true;
                    img.map.minFilter = THREE.LinearFilter; //to get rid of "The size of your texture is not powers of two" error

                    // create mesh
                    var lineCount = texture.image.height / lineHeight;
                    var height = lineCount * labelsCoeff;
                    var width =  texture.image.width / lineHeight * labelsCoeff;

                    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), img);
                    mesh.overdraw = true;
                    mesh.visible = false;

                    //add to scene and remember
                    scene.add(mesh);
                    highlightedLabels[interaction.id] = mesh;
                    allObjects.push(mesh);
                    resolve(mesh);
                },
                // Function called when download progresses
                function (xhr) {
                },
                // Function called when download errors
                function (xhr) {
                    console.error('An error happened when loading portrait: ' + xhr);
                    reject(new Error('Could not load portrait: ' + xhr));
                }
            );
        }));
    };

    // -----------------------------------
    // Setting positions
    // -----------------------------------
    this.setPositions = function(members, socialInteractions){
        var x, y, z;
        //positions of social interaction
        for (var i = 0; i < socialInteractions.length; ++i) { //iterate over interaction types
            //for (j = 0; j < socialInteractions[i].types.length; ++j) {   //iterate over element in interaction group
            x = socialInteractions[i].position.x;
            y = socialInteractions[i].position.y;
            z = socialInteractions[i].position.z;

            labels[socialInteractions[i].id].position.set(x, y, z);
            labels[socialInteractions[i].id].__dirtyPosition = true;
            labels[socialInteractions[i].id].data = socialInteractions[i];

            highlightedLabels[socialInteractions[i].id].position.set(x, y, z);
            highlightedLabels[socialInteractions[i].id].__dirtyPosition = true;
            highlightedLabels[socialInteractions[i].id].data = socialInteractions[i];
            //}
        }
        //positions of people
        for (var i = 0; i < members.length; ++i) {
            x = members[i].position.x;
            y = members[i].position.y;
            z = members[i].position.z;
            portraits[members[i].name].position.set(x, y, z);
            portraitsBorders[members[i].name].position.set(x, y, z);
            portraits[members[i].name].__dirtyPosition = true;

            portraits[members[i].name].data = members[i];
        }
    };

    // -----------------------------------
    // Updating facing
    // -----------------------------------
    this.updateFacing = function (cameraPos, cameraUp) {
        for (var idKey in labels) {
            labels[idKey].up.copy(cameraUp);
            labels[idKey].lookAt(cameraPos);
        }

        for (var idKey in highlightedLabels) {
            highlightedLabels[idKey].up.copy(cameraUp);
            highlightedLabels[idKey].lookAt(cameraPos);
        }

        for (var nameKey in portraits) {
            portraits[nameKey].up.copy(cameraUp);
            portraits[nameKey].lookAt(cameraPos);
        }
        for (var nameKey in portraitsBorders) {
            portraitsBorders[nameKey].up.copy(cameraUp);
            portraitsBorders[nameKey].lookAt(cameraPos);
        }
    };

    // -----------------------------------
    // Switching objects between spheres
    // -----------------------------------
    this.switchSpheres = function (radV) {
        var finished = false;

        if (spheresState === spheresStateEnum.NORMAL) {
            for (var idKey in labels) {
                var deltaRad = new THREE.Vector3().copy(labels[idKey].position).normalize().multiplyScalar(radV);
                labels[idKey].position.add(deltaRad);

                if (labels[idKey].position.length() >= this._rb) {
                    spheresState = spheresStateEnum.SWITCHED;
                    finished = true;
                }
            }

            for (var idKey in highlightedLabels) {
                var deltaRad = new THREE.Vector3().copy(highlightedLabels[idKey].position).normalize().multiplyScalar(radV);
                highlightedLabels[idKey].position.add(deltaRad);

                if (highlightedLabels[idKey].position.length() >= this._rb) {
                    spheresState = spheresStateEnum.SWITCHED;
                    finished = true;
                }
            }

            for (var nameKey in portraits) {
                var deltaRad = new THREE.Vector3().copy(portraits[nameKey].position).normalize().multiplyScalar(-radV);
                portraits[nameKey].position.add(deltaRad);

                if (portraits[nameKey].position.length() <= this._ra) {
                    spheresState = spheresStateEnum.SWITCHED;
                    finished = true;
                }
            }

            for (var nameKey in portraitsBorders) {
                var deltaRad = new THREE.Vector3().copy(portraitsBorders[nameKey].position).normalize().multiplyScalar(-radV);
                portraitsBorders[nameKey].position.add(deltaRad);

                if (portraitsBorders[nameKey].position.length() <= this._ra) {
                    spheresState = spheresStateEnum.SWITCHED;
                    finished = true;
                }
            }

        } else {
            for (var idKey in labels) {
                var deltaRad = new THREE.Vector3().copy(labels[idKey].position).normalize().multiplyScalar(-radV);
                labels[idKey].position.add(deltaRad);

                if (labels[idKey].position.length() <= this._ra) {
                    spheresState = spheresStateEnum.NORMAL;
                    finished = true;
                }
            }

            for (var idKey in highlightedLabels) {
                var deltaRad = new THREE.Vector3().copy(highlightedLabels[idKey].position).normalize().multiplyScalar(-radV);
                highlightedLabels[idKey].position.add(deltaRad);

                if (highlightedLabels[idKey].position.length() <= this._ra) {
                    spheresState = spheresStateEnum.NORMAL;
                    finished = true;
                }
            }

            for (var nameKey in portraits) {
                var deltaRad = new THREE.Vector3().copy(portraits[nameKey].position).normalize().multiplyScalar(radV);
                portraits[nameKey].position.add(deltaRad);

                if (portraits[nameKey].position.length() >= this._rb) {
                    spheresState = spheresStateEnum.NORMAL;
                    finished = true;
                }
            }

            for (var nameKey in portraitsBorders) {
                var deltaRad = new THREE.Vector3().copy(portraitsBorders[nameKey].position).normalize().multiplyScalar(radV);
                portraitsBorders[nameKey].position.add(deltaRad);

                if (portraitsBorders[nameKey].position.length() >= this._rb) {
                    spheresState = spheresStateEnum.NORMAL;
                    finished = true;
                }
            }
        }
        if(finished && typeof currActive !== 'undefined' && currActive !== null)
            this._ativateEdges();

        return finished;
    };

    this.initSwitching = function () {
        var currEdges = spheresState == spheresStateEnum.NORMAL ? edges : switchedEdges;

        this._deactivateAll(true);
    };

    this._ativateEdges = function(){
        this._activateNew();
    };

    // -----------------------------------
    // Activate object and show edges
    // -----------------------------------
    this.intersect = function (mousePos, camera, scope, syncSRV) {  //vector2D, camera
        raycaster.setFromCamera(mousePos, camera);
        var intersects = raycaster.intersectObjects(allObjects);
        if (intersects.length > 0) {
            //highlight nearest and expand edges
            this._deactivateAll();

            currActive = intersects[0].object.data;

            this._activateNew();
            if (currActive.typeName != null) {
                syncSRV.syncSocialInteractions(currActive.typeName);
            }
            syncSRV.syncScroll(currActive);
            scope.$apply();
        } else {
            this._unblurAll();
            this._deactivateAll();
            currActive = null;
            scope.$apply();
        }
        return currActive;
    };

    this.newActive = function (node) {

        this._deactivateAll();

        currActive = node;

        this._activateNew();
    };

    this._deactivateAll = function () {
        this._deactivateAll(false);
    };

    this._deactivateAll = function (hideEdgesOnly) {
        var currEdges = spheresState == spheresStateEnum.NORMAL ? edges : switchedEdges;

        if (typeof currActive !== 'undefined' && currActive != null){
            if (typeof currEdges[currActive.name + currActive.id] === 'undefined' || currEdges[currActive.name + currActive.id] == null) {
                if(typeof portraitsBorders[currActive.name] !== 'undefined' && portraitsBorders[currActive.name] != null){
                    if(!hideEdgesOnly) {
                        portraitsBorders[currActive.name].visible = false;
                    }
                } else {
                    if(!hideEdgesOnly) {
                        labels[currActive.id].visible = true;
                        highlightedLabels[currActive.id].visible = false;
                    }
                }
            } else {
                for (var i = 0; i < currEdges[currActive.name + currActive.id].length; ++i) {
                    currEdges[currActive.name + currActive.id][i].visible = false;
                    currEdges[currActive.name + currActive.id][i].member.active = false;
                    currEdges[currActive.name + currActive.id][i].interaction.active = false;

                    if(!hideEdgesOnly) {
                        portraitsBorders[currEdges[currActive.name + currActive.id][i].member.name].visible = false;

                        labels[currEdges[currActive.name + currActive.id][i].interaction.id].visible = true;
                        highlightedLabels[currEdges[currActive.name + currActive.id][i].interaction.id].visible = false;
                    }
                }
            }
        }
    };

    this._activateNew = function () {
        this._blurAll();
        var currEdges = spheresState == spheresStateEnum.NORMAL ? edges : switchedEdges;

        if (typeof currActive !== 'undefined' && currActive != null) {
            if (typeof currEdges[currActive.name + currActive.id] === 'undefined' || currEdges[currActive.name + currActive.id] == null) {
                if(typeof portraitsBorders[currActive.name] !== 'undefined' && portraitsBorders[currActive.name] != null){
                    portraitsBorders[currActive.name].visible = true;
                    portraits[currActive.name].material.opacity = 1.0;
                } else {
                    labels[currActive.id].visible = false;
                    highlightedLabels[currActive.id].visible = true;
                }
            } else {
                for (var i = 0; i < currEdges[currActive.name + currActive.id].length; ++i) {
                    currEdges[currActive.name + currActive.id][i].visible = true;
                    currEdges[currActive.name + currActive.id][i].member.active = true;
                    currEdges[currActive.name + currActive.id][i].interaction.active = true;
                    portraits[currEdges[currActive.name + currActive.id][i].member.name].material.opacity = 1.0;
                    portraitsBorders[currEdges[currActive.name + currActive.id][i].member.name].visible = true;

                    labels[currEdges[currActive.name + currActive.id][i].interaction.id].visible = false;
                    highlightedLabels[currEdges[currActive.name + currActive.id][i].interaction.id].visible = true;
                }
            }
        }
    };

    this._unblurAll = function () {
        this._blur(1.0);
    };

    this._blurAll = function () {
        this._blur(0.50);
    };

    this._blur = function(opacity){
        for (var idKey in labels) {
            labels[idKey].material.opacity = opacity;
        }

        for (var nameKey in portraits) {
            portraits[nameKey].material.opacity = opacity;
        }
    };

    // -----------------------------------
    // Creating edges
    // -----------------------------------

    this.createEdges = function (scene, members, socialInteractions, adjacencyMatrix) {
        var material = new MeshLineMaterial( {
            color: new THREE.Color(0xff9900),
            lineWidth: 0.4,
            transparent: true,
            opacity: 0.8
        });

        for (var i = 0; i < members.length; ++i) {
            for (var j = 0; j < socialInteractions.length; ++j) {
                if (adjacencyMatrix[members[i].id][socialInteractions[j].id]) {
                    var geometry = new THREE.Geometry();

                    var x1 = members[i].position.x;
                    var y1 = members[i].position.y;
                    var z1 = members[i].position.z;

                    var x2 = socialInteractions[j].position.x;
                    var y2 = socialInteractions[j].position.y;
                    var z2 = socialInteractions[j].position.z;

                    geometry.vertices.push(
                        new THREE.Vector3(x1, y1, z1),
                        new THREE.Vector3(x2, y2, z2)
                    );
                    var meshLine = new MeshLine();
                    meshLine.setGeometry( geometry );
                    var line = new THREE.Mesh( meshLine.geometry, material );
                    line.visible = false;
                    scene.add(line);

                    line.member = members[i];
                    line.interaction = socialInteractions[j];

                    if (edges[socialInteractions[j].name + socialInteractions[j].id] == null)
                        edges[socialInteractions[j].name + socialInteractions[j].id] = [];
                    edges[socialInteractions[j].name + socialInteractions[j].id].push(line);

                    if (edges[members[i].name + members[i].id] == null)
                        edges[members[i].name + members[i].id] = [];
                    edges[members[i].name + members[i].id].push(line);


                    //switched edges

                    geometry = new THREE.Geometry();
                    var dRadius = this._rb - this._ra;

                    var memberVector = new THREE.Vector3(x1, y1, z1);
                    var deltaRad = new THREE.Vector3().copy(memberVector).normalize().multiplyScalar(-dRadius);
                    memberVector.add(deltaRad);

                    var interactionVector = new THREE.Vector3(x2, y2, z2);
                    deltaRad = new THREE.Vector3().copy(interactionVector).normalize().multiplyScalar(dRadius);
                    interactionVector.add(deltaRad);

                    geometry.vertices.push(
                        memberVector,
                        interactionVector
                    );
                    meshLine = new MeshLine();
                    meshLine.setGeometry( geometry );
                    line = new THREE.Mesh( meshLine.geometry, material );
                    line.visible = false;
                    scene.add(line);

                    line.member = members[i];
                    line.interaction = socialInteractions[j];

                    if (switchedEdges[socialInteractions[j].name + socialInteractions[j].id] == null)
                        switchedEdges[socialInteractions[j].name + socialInteractions[j].id] = [];
                    switchedEdges[socialInteractions[j].name + socialInteractions[j].id].push(line);

                    if (switchedEdges[members[i].name + members[i].id] == null)
                        switchedEdges[members[i].name + members[i].id] = [];
                    switchedEdges[members[i].name + members[i].id].push(line);
                }
            }
        }
    };

    // -----------------------------------
    // Creating spheres
    // -----------------------------------

    this.createSpheres = function(scene) {
        var smallerFactor = 10;
        var biggerFactor = 20;
        var material = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x0D6478 });
        var material2 = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x0B912E });
        var model = this._createModel();

        var smallerSphere = this._translateModel(model, this._ra);

        var biggerSphere = this._translateModel(model, this._rb);

        material.transparent = true;
        material.opacity = 0.5;
        material2.transparent = true;
        material2.opacity = 0.4;

        var smallerMesh = new THREE.Mesh(smallerSphere, material);
        var biggerMesh = new THREE.Mesh(biggerSphere, material2);

        scene.add(smallerMesh);
        scene.add(biggerMesh);
    };

    this._createModel = function() {
        //model creation
        var geometry = new THREE.Geometry();
        var i, j, face;
        t = (1.0 + Math.sqrt(5.0)) / 2.0;

        var isohedronVertices = [
            -1, t, 0, 1, t, 0,
            -1, -t, 0, 1, -t, 0,
            0, -1, t, 0, 1, t,
            0, -1, -t, 0, 1, -t,
            t, 0, -1, t, 0, 1,
            -t, 0, -1, -t, 0, 1
        ];

        for (i = 0; i < isohedronVertices.length; i += 3) {
            this._addVertex(
                isohedronVertices[i],
                isohedronVertices[i + 1],
                isohedronVertices[i + 2],
                geometry);
        }

        geometry.faces.push(new THREE.Face3(0, 11, 5));
        geometry.faces.push(new THREE.Face3(0, 5, 1));
        geometry.faces.push(new THREE.Face3(0, 1, 7));
        geometry.faces.push(new THREE.Face3(0, 7, 10));
        geometry.faces.push(new THREE.Face3(0, 10, 11));

        geometry.faces.push(new THREE.Face3(1, 5, 9));
        geometry.faces.push(new THREE.Face3(5, 11, 4));
        geometry.faces.push(new THREE.Face3(11, 10, 2));
        geometry.faces.push(new THREE.Face3(10, 7, 6));
        geometry.faces.push(new THREE.Face3(7, 1, 8));

        geometry.faces.push(new THREE.Face3(3, 9, 4));
        geometry.faces.push(new THREE.Face3(3, 4, 2));
        geometry.faces.push(new THREE.Face3(3, 2, 6));
        geometry.faces.push(new THREE.Face3(3, 6, 8));
        geometry.faces.push(new THREE.Face3(3, 8, 9));

        geometry.faces.push(new THREE.Face3(4, 9, 5));
        geometry.faces.push(new THREE.Face3(2, 4, 11));
        geometry.faces.push(new THREE.Face3(6, 2, 10));
        geometry.faces.push(new THREE.Face3(8, 6, 7));
        geometry.faces.push(new THREE.Face3(9, 8, 1));

        for (var i = 0; i < this._recursionLevel; i++) {
            var length = geometry.faces.length;
            var faces2 = [];
            for (var j = 0; j < length; j++) {

                var face = geometry.faces[j];

                var v1 = geometry.vertices[face.a];
                var v2 = geometry.vertices[face.b];
                var v3 = geometry.vertices[face.c];

                var midPoint1 = this._createGetMiddlePointIndex(v1, v2, geometry);
                var midPoint2 = this._createGetMiddlePointIndex(v2, v3, geometry);
                var midPoint3 = this._createGetMiddlePointIndex(v3, v1, geometry);

                faces2.push(new THREE.Face3(face.a, midPoint1, midPoint3));
                faces2.push(new THREE.Face3(face.b, midPoint2, midPoint1));
                faces2.push(new THREE.Face3(face.c, midPoint3, midPoint2));
                faces2.push(new THREE.Face3(midPoint1, midPoint2, midPoint3));
            }
            geometry.faces = faces2;
        }

        geometry.computeFaceNormals();

        return geometry;
    };

    this._createGetMiddlePointIndex = function(p1, p2, geometry) {
        var x = (p1.x + p2.x) / 2;
        var y = (p1.y + p2.y) / 2;
        var z = (p1.z + p2.z) / 2;

        var point = this._addVertex(x, y, z, geometry);

        return geometry.vertices.indexOf(point);
    };

    this._addVertex = function(x, y, z, geometry) {

        var placeOnSphere = Math.sqrt(x * x + y * y + z * z);
        x = x * t / placeOnSphere;
        y = y * t / placeOnSphere;
        z = z * t / placeOnSphere;

        var point = new THREE.Vector3(x, y, z);

        if (geometry.vertices.indexOf(point) === -1)
            geometry.vertices.push(point);

        return point;
    };

    this._translateModel = function(geometry, factor) {
        for (i = 0; i < geometry.vertices.length; i++) {
            var x = geometry.vertices[i].x;
            var y = geometry.vertices[i].y;
            var z = geometry.vertices[i].z;
            var length = math.sqrt(x * x + y * y + z * z);
            var scale = factor / length;

            geometry.vertices[i].x *= scale;
            geometry.vertices[i].y *= scale;
            geometry.vertices[i].z *= scale;
        }

        return new THREE.BufferGeometry().fromGeometry(geometry);
    };

}

// -----------------------------------
// Camera
// -----------------------------------
function MovingCameraClass() {
    this.mouseX = 0;
    this.mouseY = 0;

    this.up = new THREE.Vector3(0, 1, 0);
    this.position = new THREE.Vector3(0, 0, -200);

    this.maginify = function (dRadius) {
        var deltaRad = new THREE.Vector3().copy(this.position).normalize().multiplyScalar(-dRadius);
        this.position.add(deltaRad);
    };

    this.rotate = function (dTheta, dPhi) {
        //rotate vertiacally
        var rotationAxis = new THREE.Vector3().crossVectors(this.position, this.up).normalize();
        var rotationMatrix = new THREE.Matrix4().makeRotationAxis(rotationAxis, dPhi);
        this.position.applyMatrix4(rotationMatrix);
        this.up.crossVectors(rotationAxis, this.position).normalize();

        //rotate horizontally
        rotationMatrix = new THREE.Matrix4().makeRotationAxis(this.up, -dTheta);
        this.position.applyMatrix4(rotationMatrix);
    };

    this.rotateTo = function (dest, angleV) {
        var angle = this.position.angleTo(dest);
        if (angle < angleV)
            return true;

        var rotationAxis = new THREE.Vector3().crossVectors(this.position, dest).normalize();
        var rotationMatrix = new THREE.Matrix4().makeRotationAxis(rotationAxis, angleV);
        this.position.applyMatrix4(rotationMatrix);
        this.up.projectOnPlane(this.position).normalize();
        return false;
    };
}
