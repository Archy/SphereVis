package com.sphereviz.services.relationship;

import com.sphereviz.data.graph.NewPeopleRepository;
import com.sphereviz.data.graph.SocialInteractionsRepository;
import com.sphereviz.data.relationship.RelationshipRepository;
import com.sphereviz.model.graph.Person;
import com.sphereviz.model.graph.SocialInteraction;
import com.sphereviz.model.graph.VisEntity;
import com.sphereviz.model.relationship.Relationship;
import com.sphereviz.model.relationship.RelationshipImpl;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;


@Component
public class RelationshipManagerImpl implements RelationshipManager {
    private static final Logger log = Logger.getLogger(RelationshipManagerImpl.class);

    private final RelationshipRepository relationshipRepository;
    private final NewPeopleRepository peopleRepository;
    private final SocialInteractionsRepository interactionsRepository;

    private List<? extends Relationship> relationships;


    @Inject
    public RelationshipManagerImpl(@NotNull final RelationshipRepository relationshipRepository,
                                   @NotNull final NewPeopleRepository peopleRepository,
                                   @NotNull final SocialInteractionsRepository interactionsRepository) {
        this.relationshipRepository = relationshipRepository;
        this.peopleRepository = peopleRepository;
        this.interactionsRepository = interactionsRepository;
    }

    @Override
    public void loadData(){
        relationships = relationshipRepository.findAll();
    }

    @Override
    public boolean relationshipExists(final VisEntity e1, final VisEntity e2) {
        long personID, interactionID;

        if(e1 instanceof Person && e2 instanceof SocialInteraction){
            personID = e1.getId();
            interactionID = e2.getId();
        } else if(e2 instanceof Person && e1 instanceof SocialInteraction) {
            personID = e2.getId();
            interactionID = e1.getId();
        } else
            return  false;

        return relationshipExists(personID, interactionID);
    }

    @Override
    public boolean relationshipExists(final long personID, final long interactionID){
        return  relationships.stream()
                .anyMatch(relationship ->
                        (relationship.getPersonId() == personID) && (relationship.getInteractionId() == interactionID)
                );
    }

    @Override
    public boolean updateRelationship(final long interactionID, final long personID, final boolean newRelationshipStatus) {
        boolean currentRelation = relationshipExists(personID, interactionID);
        if(currentRelation == newRelationshipStatus)
            return currentRelation;

        if(!currentRelation){
            //create relationship
            final Person person = peopleRepository.findById(personID);
            final SocialInteraction interaction = interactionsRepository.findById(interactionID);

            final RelationshipImpl relationship = new RelationshipImpl(person, interaction);
            relationshipRepository.saveAndFlush(relationship);

            log.info("Relationship: person(" + personID +") - interaction(" + interactionID + ") has been created");
            return true;
        } else {
            //delete relationship
            final RelationshipImpl relationship
                    = relationshipRepository.findByPersonIdAndInteractionId(personID, interactionID);
            relationshipRepository.delete(relationship);

            log.info("Relationship: person(" + personID +") - interaction(" + interactionID + ") has been deleted");
            return false;
        }
    }
}
