package com.sphereviz.model.graph;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.vecmath.Vector3d;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(
        name = "SOCIAL_INTERACTION"
)
public class SocialInteractionImpl implements SocialInteraction {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "POSITION")
    private Vector3d position;
    @ManyToOne(
            targetEntity = SocialInteractionGroupImpl.class,
            fetch = FetchType.EAGER
    )

    @JoinColumn(
            name = "SOCIAL_INTERACTION_GROUP"
    )
    private SocialInteractionGroup group;
    @ManyToOne(
            targetEntity = SocialInteractionImpl.class,
            fetch = FetchType.EAGER
    )
    @JoinColumn(
            name = "PARENT_SOCIAL_INTERACTION"
    )
    private SocialInteraction parentInteraction;
    @OneToMany(
            targetEntity = SocialInteractionImpl.class,
            fetch = FetchType.EAGER,
            mappedBy = "parentInteraction"
    )
    private Set<SocialInteraction> subInteractions = new HashSet<>();


    public SocialInteractionImpl() {
    }

    public SocialInteractionImpl(String name) {
        this.name = name;
    }

    public SocialInteractionImpl(String name, SocialInteractionGroup group) {
        this.name = name;
        this.group = group;
    }

    public SocialInteractionImpl(String name, SocialInteraction parentInteraction) {
        this.name = name;
        this.parentInteraction = parentInteraction;
    }

    public SocialInteractionImpl(String name, Vector3d position, SocialInteractionGroup group) {
        this.name = name;
        this.position = position;
        this.group = group;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Vector3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3d position) {
        this.position = position;
    }

    public SocialInteractionGroup getGroup() {
        return group;
    }

    @Override
    public boolean hasSubInteractions() {
        return !subInteractions.isEmpty();
    }

    public void setGroup(SocialInteractionGroup group) {
        this.group = group;
    }

    public SocialInteraction getParentInteraction() {
        return parentInteraction;
    }

    public void setParentInteraction(SocialInteraction parentInteraction) {
        this.parentInteraction = parentInteraction;
    }

    @Override
    @NotNull
    public Set<SocialInteraction> getSubInteractions() {
        return subInteractions;
    }

    public void setSubInteractions(Set<SocialInteraction> subInteractions) {
        this.subInteractions = subInteractions;
    }

    public void addSubInteraction(@NotNull final SocialInteraction subInteraction){
        subInteractions.add(subInteraction);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocialInteractionImpl that = (SocialInteractionImpl) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
